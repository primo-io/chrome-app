var RotatingCube = function() {

    this.el = document.body.querySelector("#worldcontainer");

    width = this.el.offsetWidth;
    height = this.el.offsetHeight;

    this.scene = new THREE.Scene();
    this.container = new THREE.Object3D();
    this.scene.add(this.container);

    /* world */
    this.renderer = new THREE.WebGLRenderer({
        antialias: true
    });


    /* renderer */
    this.renderer.setClearColor(0x592041);
    this.renderer.setSize(width, height);
    this.renderer.shadowMapEnabled = true;
    this.renderer.shadowMapType = THREE.PCFSoftShadowMap;
    // this.renderer.shadowMapDebug = true;

    var r = height / width;
    this.worldScale = 1.5;

    /* camera */
    this.camera = new THREE.OrthographicCamera(-this.worldScale, this.worldScale, this.worldScale * r, -this.worldScale * r, -10, 60);
    // this.camera = new THREE.PerspectiveCamera(50, width / height, 1, 10000);
    this.camera.position.x = 2;
    this.camera.position.y = 2;
    this.camera.position.z = 2;
    this.camera.lookAt(this.container.position);




    var light = new THREE.DirectionalLight(0x999999);

    this.scene.add(light);

    light = new THREE.AmbientLight(0x999999, .2);
    this.scene.add(light);


    /* dom */
    this.el.appendChild(this.renderer.domElement);


    this.cubetto = new Cuby();
    this.scene.add(this.cubetto);


}


RotatingCube.prototype.update = function() {

    this.renderer.render(this.scene, this.camera);
}

var Cuby = function(options) {

    THREE.Object3D.apply(this);

    var self = this;
    self.cubettoTexture = new THREE.Texture(document.body.querySelector("#cubettotexture"));
    self.cubettoTexture.needsUpdate = true;
    self.init();

}

Cuby.prototype = Object.create(THREE.Object3D.prototype);
Cuby.prototype.constructor = Cuby;


Cuby.prototype.init = function() {

    var geometry = new THREE.BoxGeometry(.8, .8, .8);
    var g = geometry.faceVertexUvs[0];

    var topFace = [new THREE.Vector2(0, .666), new THREE.Vector2(.333, .666), new THREE.Vector2(.333, 1), new THREE.Vector2(0, 1)];
    var empty = [new THREE.Vector2(.333, 0), new THREE.Vector2(.666, 0), new THREE.Vector2(.666, .333), new THREE.Vector2(.333, .333)];

    this.sadFace = [new THREE.Vector2(0, .333), new THREE.Vector2(.333, .333), new THREE.Vector2(.333, .666), new THREE.Vector2(0, .666)];
    this.happyFace = [new THREE.Vector2(.333, .333), new THREE.Vector2(.666, .333), new THREE.Vector2(.666, .666), new THREE.Vector2(.333, .666)];
    this.surprisedFace = [new THREE.Vector2(0, 0), new THREE.Vector2(.333, 0), new THREE.Vector2(.333, .333), new THREE.Vector2(0, .333)];


    g[2] = [empty[3], empty[0], empty[2]];
    g[3] = [empty[0], empty[1], empty[2]];

    // top
    g[4] = [topFace[3], topFace[0], topFace[2]];
    g[5] = [topFace[0], topFace[1], topFace[2]];

    //bottom
    g[6] = [empty[3], empty[0], empty[2]];
    g[7] = [empty[0], empty[1], empty[2]];

    g[8] = [empty[3], empty[0], empty[2]];
    g[9] = [empty[0], empty[1], empty[2]];


    g[10] = [empty[3], empty[0], empty[2]];
    g[11] = [empty[0], empty[1], empty[2]];


    var materialb = new THREE.MeshLambertMaterial({
        map: this.cubettoTexture,
        color: 0xeccba6,
        ambient: 0xeccba6
    });

    var materialDebug = new THREE.MeshBasicMaterial();
    this.cube = new THREE.Mesh(geometry, materialb);
    this.cube.castShadow = true;
    this.changeFace(this.happyFace);

    this.add(this.cube);

    this.rotateOnAxis(new THREE.Vector3(0, 1, 0), -Math.PI * .25);
    this.rotateOnAxis(new THREE.Vector3(0, 0, 1), Math.PI * .20);

}
Cuby.prototype.changeFace = function(uv) {
    var g = this.cube.geometry.faceVertexUvs[0];
    g[0] = [uv[3], uv[0], uv[2]];
    g[1] = [uv[0], uv[1], uv[2]];
    this.cube.geometry.uvsNeedUpdate = true;
}