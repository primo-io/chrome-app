 var l;


 window.onload = function() {
     l = new LessonView();
 }

 LessonView = function() {

     this.blocklyFrame = document.getElementById("blocklyFrame");
     window.addEventListener('message', this.onMessageReceived.bind(this));

     this.sc = new SerialConnector({
         onReceive: this.onReceive.bind(this),
         onLostConnection: this.onLostConnection.bind(this),
         onAvailable: this.onAvailable.bind(this)
     });

     this.buttons = new Buttons();
     this.buttons.runBtn.onclick = this.onRunBtnclicked.bind(this);
     this.buttons.testBtn.onclick = this.onTestBtnclicked.bind(this);

     this.isRunningTest = false;
     this.isRunningOnCubetto = false;

     this.lessonTitle = document.querySelector("#lessonname");
     this.menuLinks = document.querySelectorAll("#lessonlist li a");


     for (var i = 0; i < this.menuLinks.length; i++) {
         var li = this.menuLinks[i];
         li.onmousedown = this.onClicked.bind(this, li);
     }



     this.sounds = {};
     this.soundsNames = ['motor'];

     for (var i = 0; i < this.soundsNames.length; i++) {
         var n = this.soundsNames[i];
         this.sounds[n] = new Wad({
             source: "sounds/" + n + ".mp3"
         });
     };




     this.piano = new Wad({
         source: 'sine'
     });

     // this.changeFrame(1);
 }

 LessonView.prototype = {

     onClicked: function(li, event) {

         event.preventDefault();

         for (var i = 0; i < this.menuLinks.length; i++) {
             this.menuLinks[i].className = "";
         }

         li.className = "active";

         setTimeout(function() {

             this.lessonTitle.textContent = li.title;
             this.changeFrame(li.dataset.link);

         }.bind(this), 200);


     },

     onAvailable: function(c) {

         console.log("onAvailable");

         this.buttons.setAvailable(true, c);
         this.postMessage({
             command: "available"
         });
     },

     onLostConnection: function() {

         this.buttons.setAvailable(false);
         this.postMessage({
             command: "lostConnection"
         });

     },

     onReceive: function() {

         this.postMessage({
             data: this.sc.serialValues,
             command: "serial",
         });


     },

     onErorr: function(data) {

         console.log(data);

     },

     changeFrame: function(n) {
         this.blocklyFrame.src = n;

         setTimeout(function() {
             this.postMessage({
                 command: "start"
             });
         }.bind(this), 200)
     },


     postMessage: function(data) {
         this.blocklyFrame.contentWindow.postMessage(data, '*');
     },

     onMessageReceived: function(event) {



         if (event.data && event.data.command) {

             switch (event.data.command) {

                 case 'sound':
                     this.playSound(event.data.data);
                     break;

                 case 'enableButtons':
                     this.buttons.enable();
                     break;

                 case 'disableButtons':
                     this.buttons.disable();
                     break;

                 case 'finish':

                     if (this.isRunningTest) {
                         this.isRunningTest = false;
                         this.buttons.setButtonLabelToStop(false, this.buttons.testBtn);
                     }

                     if (this.isRunningOnCubetto) {
                         this.isRunningOnCubetto = false;
                         this.buttons.setButtonLabelToStop(false, this.buttons.runBtn);
                     }

                     break;

                 case 'serial':
                     console.log(event.data)
                     this.sc.send(event.data.data);
                     break;

             }
         }
     }



 }

 LessonView.prototype.playSound = function(d) {
     console.log("playing", d)

     // d.data 
     // 

     var name = d.name;
     if (name.match(/note/)) {

         var r = d.duration / 1000;

         if (this.isRunningTest) {
             this.piano.play({
                 pitch: name.substr(4),
                 env: {
                     hold: r,
                     release: r
                 }
             })
         }

     } else {

         var duration = d.duration;
         if (!this.sounds[name]) return;

         var sound = this.sounds[name];
         if (duration) {

             console.log(duration)

             sound.play({
                 env: {
                     hold: duration / 1000
                 }
             });

         } else {

             sound.play();

         }

     }

 }

 LessonView.prototype.stopAllSounds = function(e) {

     for (var i in this.sounds) {
         this.sounds[i].stop();
     };
 }

 LessonView.prototype.onRunBtnclicked = function(e) {


     this.isRunningOnCubetto = !this.isRunningOnCubetto;

     this.buttons.setButtonLabelToStop(this.isRunningOnCubetto, this.buttons.runBtn);

     if (this.isRunningOnCubetto) {
         this.postMessage({
             command: "run"
         });
     } else {
         this.postMessage({
             command: "stop"
         });

     }

 }

 LessonView.prototype.onTestBtnclicked = function(e) {


     this.isRunningTest = !this.isRunningTest;

     this.buttons.setButtonLabelToStop(this.isRunningTest, this.buttons.testBtn);

     if (this.isRunningTest) {

         this.postMessage({
             command: "test"
         });


     } else {

         this.postMessage({
             command: "stop"
         });

         this.stopAllSounds();


     }

 }