/*


Firmware for radio and serial communication (Blockly)

To be used with Cubetto 1.1


Radio pairing
This code includes a mechanism for Interface/Cubetto Robot to pair.
When a Cubetto Robot is powered-on, it is unpaired and will accept input from
any Interface.  When an Interface is powered on, it generates a 32-bit random
number to use as its session Unique ID (UID).
Every radio message that an Interface sends includes this UID.
A Cubetto Robot, on receiving its first radio message, records this UID,
and subsequently ignores messages from any other UID.
A devices's UID is forgotten when it is powered off.


Serial communication
Commands over serial are listed in the loop function


@author Danilo Di Cuia - danilo@primo.io
@author Claudio Indellicati
@author Matteo Loglio - matteo@primo.io




*/

////////////////////////////////////////////////////////////////////////////////


#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <printf.h>
#include <AccelStepper.h>

#include "Primo.h"
#include "sound.h"


////////////////////////////////////////////////////////////////////////////////

#define PRIMO_DEBUG_MODE

#ifdef PRIMO_DEBUG_MODE
#define debugPrintf printf
#define debugMessage dumpMessage
#else
#define debugPrintf(...) ((void) 0)
#define debugMessage(...) ((void) 0)
#endif

////////////////////////////////////////////////////////////////////////////////

// Set up nRF24L01 radio on SPI bus pins 7 (CE) and 8 (CSN)
RF24 radio(7, 8);

// Flag to signal between interrupt handler and main event loop
volatile bool noMessageReceived = true;

CommandsMessage commandsMsg;

////////////////////////////////////////////////////////////////////////////////

#define PRIMO_LEFT_STEPPER_PIN_1 12
#define PRIMO_LEFT_STEPPER_PIN_2 10
#define PRIMO_LEFT_STEPPER_PIN_3 11
#define PRIMO_LEFT_STEPPER_PIN_4 9

#define PRIMO_RIGHT_STEPPER_PIN_1 6
#define PRIMO_RIGHT_STEPPER_PIN_2 4
#define PRIMO_RIGHT_STEPPER_PIN_3 5
#define PRIMO_RIGHT_STEPPER_PIN_4 3

#define PRIMO_STEPPER_MAX_SPEED    1000
#define PRIMO_STEPPER_ACCELERATION 500

#define PRIMO_STEPPER_FORWARD_STEPS 2700
#define PRIMO_STEPPER_TURN_STEPS    1170

// Define a stepper and the pins it will use
AccelStepper leftStepper(AccelStepper::HALF4WIRE, PRIMO_LEFT_STEPPER_PIN_1, PRIMO_LEFT_STEPPER_PIN_2, PRIMO_LEFT_STEPPER_PIN_3, PRIMO_LEFT_STEPPER_PIN_4);
AccelStepper rightStepper(AccelStepper::HALF4WIRE, PRIMO_RIGHT_STEPPER_PIN_1, PRIMO_RIGHT_STEPPER_PIN_2, PRIMO_RIGHT_STEPPER_PIN_3, PRIMO_RIGHT_STEPPER_PIN_4);

//check inactivity and beep if inactive for too long
//time limit is 2 minutes (120 seconds)
long inactiveTimeLimit = 120000;
long timeStamp = 0;


boolean streamingValues = false;


////////////////////////////////////////////////////////////////////////////////

void setup()
{
  pinMode(PRIMO_BUZZER_PIN, OUTPUT);

  pinMode(PRIMO_LEFT_STEPPER_PIN_1, OUTPUT);
  pinMode(PRIMO_LEFT_STEPPER_PIN_2, OUTPUT);
  pinMode(PRIMO_LEFT_STEPPER_PIN_3, OUTPUT);
  pinMode(PRIMO_LEFT_STEPPER_PIN_4, OUTPUT);

  pinMode(PRIMO_RIGHT_STEPPER_PIN_1, OUTPUT);
  pinMode(PRIMO_RIGHT_STEPPER_PIN_2, OUTPUT);
  pinMode(PRIMO_RIGHT_STEPPER_PIN_3, OUTPUT);
  pinMode(PRIMO_RIGHT_STEPPER_PIN_4, OUTPUT);

  // IMPORTANT Stepper outputs are disabled here to minimise power usage whilst
  // stationary, as they are automatically enabled in the AccelStepper class
  // constructor
  leftStepper.disableOutputs();
  rightStepper.disableOutputs();

  leftStepper.setMaxSpeed(PRIMO_STEPPER_MAX_SPEED);
  rightStepper.setMaxSpeed(PRIMO_STEPPER_MAX_SPEED);
  leftStepper.setAcceleration(PRIMO_STEPPER_ACCELERATION);
  rightStepper.setAcceleration(PRIMO_STEPPER_ACCELERATION);

  Serial.begin(115200);
  printf_begin();
  debugPrintf("Cubetto Playset - Cubetto Robot\n\rVersion %s\n\r", PRIMO_CUBETTO_PLAYSET_VERSION);

  //
  // Setup and configure RF radio module
  //

  radio.begin();
  //radio.setPALevel(RF24_PA_LOW);

  // Use the ACK payload feature (ACK payloads are dynamic payloads)
  radio.enableAckPayload();
  radio.enableDynamicPayloads();

  // Open pipes to other node for communication
  radio.openWritingPipe(PRIMO_CUBETTO2INTERFACE_PIPE_ADDRESS);
  radio.openReadingPipe(1, PRIMO_INTERFACE2CUBETTO_PIPE_ADDRESS);

  radio.startListening();

  // Add an ACK packet for the next time around
  writeAckPayload();

  // Dump the configuration of the RF module for debugging
  radio.printDetails();
  delay(50);

  // Attach interrupt handler to interrupt #1 (using pin 2 on Arduino Leonardo)
  attachInterrupt(1, checkRadio, LOW);

  playHappyTune();
}


////////////////////////////////////////////////////////////////////////////////

void loop()
{


  if (streamingValues) {

    //output serial values
    Serial.print("#A5" + String(analogRead(A5)));
    Serial.print("\n");
  }

  if (Serial.available() > 0) {


    String string = Serial.readStringUntil('\n');
    string.trim();
    boolean oneOrMoreCommandsExecuted = false;

    String command = string.substring(0, 1);

    if (command == "N") {
      playNoteFromString(string.substring(1).c_str());
    }
    else if (command == "Z") {
      noTone(PRIMO_BUZZER_PIN);
    }
    else if (command == "R") {
      long steps = atol(string.substring(1).c_str());
      leftStepper.move(steps);
      rightStepper.move(steps);
      makeSound(4000, 50);
      oneOrMoreCommandsExecuted = true;
    }
    else if (command == "F") {
      long steps = atol(string.substring(1).c_str());
      leftStepper.move(-steps);
      rightStepper.move(steps);
      makeSound(3000, 50);
      oneOrMoreCommandsExecuted = true;
    }
    else if (command == "L") {
      long steps = atol(string.substring(1).c_str());
      leftStepper.move(-steps);
      rightStepper.move(-steps);
      makeSound(5000, 50);
      oneOrMoreCommandsExecuted = true;
    }
    else if (command == "B") {

      long steps = atol(string.substring(1).c_str());
      leftStepper.move(steps);
      rightStepper.move(-steps);
      makeSound(3000, 50);
      oneOrMoreCommandsExecuted = true;



    }

    else if (command == "M") {

      String motor = string.substring(1, 2);
      String sign = string.substring(2, 3);
      long steps = atol(string.substring(3).c_str());

      if (sign == "-") {
        steps *= -1;
      }


      if (motor == "R") {
        rightStepper.move(steps);
        makeSound(5000, 50);
        oneOrMoreCommandsExecuted = true;

      } else if (motor == "L") {
        leftStepper.move(steps);
        makeSound(5000, 50);
        oneOrMoreCommandsExecuted = true;

      }
 
    }

    else if (command == "S") {
      String value = string.substring(1, 2);
      if(value == "0") {
        streamingValues = false;
      } else {
        streamingValues = true;
      }
    }

    else if (command == "T") {
      playConnectedTune();
    }



    if (oneOrMoreCommandsExecuted) {
      playHappyTune();
      runBothSteppers();
    }
  }

  if (noMessageReceived) {
    checkInactivity();
    return;
  }

  timeStamp = millis();

  debugMessage(commandsMsg);

  executeInstructions(commandsMsg);

  noMessageReceived = true;




}





////////////////////////////////////////////////////////////////////////////////
