var rightDeg = 0;
var phase = 0;
var activeMagnet = 0;
var direction = 1;

var layout = [
    [
        235,
        61,
        0
    ],
    [
        410,
        235,
        Math.PI / 2,
    ],
    [
        235,
        413,
        Math.PI,
    ],
    [
        61,
        236,
        Math.PI + Math.PI / 2,
    ]
];


Lesson2 = function() {
    Lesson.call(this);


    this.fullyLoaded = false;
    this.ctx = document.getElementById('canvas').getContext('2d');
    this.halfWheelWidth = (544 / 2) / 2;


    this.magnetImgPlus = new Image();
    this.magnetImgPlus.src = 'images/lesson2/magnetA.png';

    this.magnetImgMinus = new Image();
    this.magnetImgMinus.src = 'images/lesson2/magnetB.png';

    this.magnetImgInactive = new Image();
    this.magnetImgInactive.src = 'images/lesson2/magnetC.png';

    this.wheel = new Image();
    this.wheel.src = 'images/lesson2/wheel.png';
    this.slider = document.body.querySelector("#slider input");
    this.draw();

    this.inactive = true;
}

Lesson2.prototype = Object.create(Lesson.prototype);
Lesson2.prototype.constructor = Lesson2;

Lesson2.prototype.onBlocklyStarted = function(c) {

    if (!this.serialMode) {

        this.inactive = false;

    }

}

Lesson.prototype.onBlocklyExecuted = function(command) {

    // if it's testing don't send message as it goes way slower than blockly

    if (this.serialMode) {
        this.sendMessage({
            command: "finish"
        });
    }

}


Lesson2.prototype.onBlocklyCommand = function(c) {


    if (!this.serialMode) {
        this.targetSteps = parseInt(c.command.data.substr(3));
        this.currentStep = 0;
        direction = c.command.data.substr(2, 1) == "+" ? 1 : -1;
        phase = 0;

        if (rightDeg != 0) {
            var speed = (this.slider.max - this.slider.value + 1) * .1;
            TweenLite.to(window, speed, {
                rightDeg: 0,
                onComplete: this.moveWheel.bind(this)
            });
        } else {
            console.log(this.slider)
            this.moveWheel();
        }

    } else {
        //edit the steps for real cubetto
        // realsteps : fakesteps = 4100 : 100
        var targetSteps = 41 * parseInt(c.command.data.substr(3));
        console.log("%c sending command ", 'background: red; color: white', c.command.data.substr(0, 3) + targetSteps);

        this.sendMessage({
            command: 'serial',
            data: c.command.data.substr(0, 3) + targetSteps
        });
    }


}

Lesson2.prototype.moveWheel = function() {


    var speed = (this.slider.max - this.slider.value + 1) * .1;

    TweenLite.to(window, speed, {
        rightDeg: rightDeg + (3.6 * direction),
        onComplete: function() {

            if (this.currentStep++ >= this.targetSteps) {

                this.deactivate();

            } else {
                phase++;
                this.moveWheel();
            }

        }.bind(this)
    });

}

Lesson2.prototype.stop = function(event) {
    Lesson.prototype.stop.call(this);
    this.panel.stopBlockly();
    this.deactivate();

}

Lesson2.prototype.deactivate = function(event) {
    TweenLite.killTweensOf(window);
    this.inactive = true;
    this.sendMessage({
        command: "finish"
    });
}
Lesson2.prototype.draw = function() {

    this.ctx.save();
    // this.ctx.translate(-10, -40);

    this.ctx.fillStyle = "#ffffff"
    this.ctx.rect(0, 0, 512, 512);
    this.ctx.fill();
    for (var i = 0; i < layout.length; i++) {
        this.ctx.save();
        this.ctx.translate(layout[i][0], layout[i][1]);
        this.ctx.rotate(layout[i][2]);


        var img;


        if ((i + phase) % 2 || this.inactive) {

            img = this.magnetImgInactive;
        } else {
            img = (i > 1) ? this.magnetImgMinus : this.magnetImgPlus;
        }

        this.ctx.drawImage(img, -109 / 2, -88 / 2, 109, 88);
        this.ctx.restore();
    }

    this.ctx.save();
    this.ctx.translate(236, 236);
    this.ctx.rotate(rightDeg / 180 * Math.PI);
    this.ctx.drawImage(this.wheel, -260 / 2, -260 / 2, 260, 260);
    this.ctx.restore();
    this.ctx.restore();


    requestAnimationFrame(this.draw.bind(this));

}


var lesson = new Lesson2();