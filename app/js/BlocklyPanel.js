/*

        Class used to send commands from blockly

        Available commands:

        Z          Mute                         silence the buzzer
        Wxyyyy     Write actuator value         x: pin number, yyyy: value
        Mxayyyy    Move motor clockwise         x: motor to move (can be either "L" for left or "R" for right), a: sign (can be + or -), yyyy: number of steps to move
        Fxxxx      Move Cubetto forward         xxxx: steps           
        Rxxxx      Rotate Cubetto  right         xxxx: steps             
        Lxxxx      Rotate Cubetto  left          xxxx: steps               
        S          Toggle sensor stream 


        */



// used for music blocks to refeactor in its own lesson when we finish the framework
var BPM = 100;

var BlocklyPanel = (function() {

    // private vars
    var interpreter;
    var highlightPause;
    var pausing = true;
    var currentTimeout;
    var completeCallback;
    var startCallback;
    var changeCallback;
    var commandCallback;
    // constants
    var DEGREES_STEPS_RATIO = Math.round(1170 / 90);
    var CMS_STEPS_RATIO = Math.round(2700 / 16);


    var sensorValue = 0;

    function initCubettoBlocks() {
        /* define and create blocks definitions */

        var commands = [{
            name: 'left',
            command: 'L',
            value: 'degrees',
            label: 'left',
            defaultValue: 90,
            color: [46, 136, 200]
        }, {
            name: 'right',
            command: 'R',
            value: 'degrees',
            label: 'right',
            defaultValue: 90,
            color: [251, 196, 98]
        }, {
            name: 'forward',
            command: 'F',
            value: 'number',
            label: 'forward',
            defaultValue: 15,
            color: [231, 64, 64]
        }, {
            name: 'backward',
            command: 'B',
            value: 'number',
            label: 'backward',
            defaultValue: 15,
            color: [5, 220, 148]
        }];

        for (var i = 0; i < commands.length; i++) {
            var c = commands[i];
            createCubettoBlock(c);
        }



        function createCubettoBlock(c) {

            // create both editable and non editable block
            Blockly.Blocks[c.name] = {
                init: function() {
                    this.valueType = c.value;
                    this.setHelpUrl('');
                    this.setColour = this.updateColour = setBlockColour.bind(this);
                    this.setColour(c.color);
                    var input = this.appendDummyInput();
                    input.appendField(new Blockly.FieldImage("img/arrow" + c.command + ".svg", 20, 20, "arrow"))
                    input.appendField(c.label);
                    this.setPreviousStatement(true);
                    this.setNextStatement(true);
                    this.setTooltip('');
                }
            }

            Blockly.Blocks[c.name + "_editable"] = {
                init: function() {

                    this.setHelpUrl('');
                    this.setColour = this.updateColour = setBlockColour.bind(this);
                    this.setColour(c.color);
                    this.valueType = c.value;

                    var input = this.appendDummyInput();
                    input.appendField(new Blockly.FieldImage("img/arrow" + c.command + ".svg", 20, 20, "arrow"))
                    input.appendField(c.label);

                    switch (c.value) {

                        case "degrees":
                            input.appendField(new Blockly.FieldAngle("90"), "VALUE");
                            input.appendField("deg");
                            break;

                        case "number":
                            input.appendField(new Blockly.FieldTextInput("10"), "VALUE");
                            input.appendField("cm");
                            break;

                    }

                    this.setPreviousStatement(true);
                    this.setNextStatement(true);
                    this.setTooltip('');

                }
            }


            Blockly.Blocks[c.name + "_variable"] = {
                init: function() {
                    this.setHelpUrl('');
                    this.setColour = this.updateColour = setBlockColour.bind(this);
                    this.setColour(c.color);
                    var input = this.appendValueInput("VALUE");
                    input.setCheck("Number")
                    input.appendField(new Blockly.FieldImage("img/arrow" + c.command + ".svg", 20, 20, "arrow"))
                    input.appendField(c.label);



                    this.setInputsInline(true);

                    var input = this.appendDummyInput();

                    switch (c.value) {

                        case "degrees":
                            input.appendField("deg");
                            break;

                        case "number":
                            input.appendField("cm");
                            break;

                    }

                    this.setPreviousStatement(true);
                    this.setNextStatement(true);
                    this.setTooltip('');
                }
            };



            // IMPORTANT wait 5 seconds after each direction command so that the board has time to execute it 

            Blockly.JavaScript[c.name] = function(block) {
                var argument0 = convertToSteps(block.valueType, c.defaultValue);
                console.log(argument0, block.valueType, c.defaultValue)
                return "sendCommand('" + c.command + argument0 + "', 5000);";
            }

            Blockly.JavaScript[c.name + "_editable"] = function(block) {
                var argument0 = convertToSteps(block.valueType, block.getFieldValue("VALUE"));
                return "sendCommand('" + c.command + argument0 + "', 5000);";
            }

            Blockly.JavaScript[c.name + "_variable"] = function(block) {
                var argument0 = convertToSteps(block.valueType, Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ASSIGNMENT) || '');
                return "sendCommand('" + c.command + argument0 + "', 5000);";
            }


            function convertToSteps(type, v) {
                return (type == "degrees" ? DEGREES_STEPS_RATIO : CMS_STEPS_RATIO) * v;
            }


        }

        // low level 

        Blockly.Blocks['move_motor'] = {

            init: function() {
                this.setColour = this.updateColour = setBlockColour.bind(this);
                this.setColour([105, 45, 85]);

                var input = this.appendDummyInput();

                input.appendField("Move ");

                input.appendField(new Blockly.FieldDropdown([
                    ["Left", "L"],
                    ["Right", "R"]
                ]), "motorName");

                input.appendField(" motor");

                input.appendField(new Blockly.FieldDropdown([
                    ["clockwise", "c"],
                    ["anit-clockwise", "ac"]
                ]), "sign");

                input.appendField(new Blockly.FieldTextInput("1000"), "steps");
                input.appendField(" steps");
                this.setPreviousStatement(true);
                this.setNextStatement(true);
                this.setTooltip('');
            }
        }

        Blockly.JavaScript['move_motor'] = function(block) {
            var motorName = block.getFieldValue('motorName');
            var steps = block.getFieldValue('steps');
            var sign = block.getFieldValue('sign') == "c" ? "+" : "-";
            console.log(motorName, steps, sign)
            return "sendCommand('M" + motorName + sign + steps + "', 5000);";

        }

        // low level single motor

        // Blockly.Blocks['move_single_motor'] = {

        //     init: function() {
        //         this.setColour = this.updateColour = setBlockColour.bind(this);
        //         this.setColour([105, 45, 85]);

        //         var input = this.appendDummyInput();

        //         input.appendField("Move motor");

        //         input.appendField(new Blockly.FieldDropdown([
        //             ["clockwise", "c"],
        //             ["anti-clockwise", "ac"]
        //         ]), "sign");

        //         input.appendField(new Blockly.FieldTextInput("1000"), "steps");
        //         input.appendField(" steps");
        //         this.setPreviousStatement(true);
        //         this.setNextStatement(true);
        //         this.setTooltip('');
        //     }
        // }


        Blockly.Blocks['move_single_motor'] = {
            init: function() {
                this.setColour = this.updateColour = setBlockColour.bind(this);
                this.setColour([105, 45, 85]);
                this.appendDummyInput()
                    .appendField("Move motor")

                this.appendDummyInput()
                    .appendField(new Blockly.FieldTextInput("100"), "steps")
                    .appendField("steps")
                    .appendField(new Blockly.FieldDropdown([
                        ["clockwise", "c"],
                        ["anticlockwise", "ac"]
                    ]), "sign");
                this.setPreviousStatement(true);
                this.setNextStatement(true);
                this.setTooltip('');
            }
        };

        Blockly.JavaScript['move_single_motor'] = function(block) {
            // var motorName = block.getFieldValue('motorName');
            var steps = block.getFieldValue('steps');
            var sign = block.getFieldValue('sign') == "c" ? "+" : "-";
            // console.log(motorName, steps, sign)
            return "sendCommand('ML" + sign + steps + "', " + (steps * 10) + ");";

        }


        // notes

        var durations = [
            ["whole", 1],
            ["half", .5],
            ["quarter", .25],
            ["eigth", .125]
        ];


        Blockly.Blocks["pause_editable"] = {
            init: function() {

                this.setHelpUrl('');
                this.setColour = this.updateColour = setBlockColour.bind(this);
                this.setColour([150, 150, 150]);

                var input = this.appendDummyInput();
                input.appendField("pause");

                input.appendField(new Blockly.FieldTextInput("10"), "VALUE");
                input.appendField("sec");

                this.setPreviousStatement(true);
                this.setNextStatement(true);
                this.setTooltip('');

            }
        }


        Blockly.Blocks["pause_variable"] = {
            init: function() {
                this.setHelpUrl('');
                this.setColour = this.updateColour = setBlockColour.bind(this);
                this.setColour([150, 150, 150]);

                var input = this.appendDummyInput();
                input.appendField("pause");

                var input = this.appendValueInput("VALUE");
                input.setCheck("Number")

                var input = this.appendDummyInput();
                input.appendField("sec");

                this.setInputsInline(true);

                this.setPreviousStatement(true);
                this.setNextStatement(true);
                this.setTooltip('');
            }
        };

        Blockly.JavaScript[c.name + "_editable"] = function(block) {
            var argument0 = convertToSteps(block.valueType, block.getFieldValue("VALUE")) * 1000;
            return "pause(" + argument0 + ");";
        }

        Blockly.JavaScript[c.name + "_variable"] = function(block) {
            var argument0 = convertToSteps(block.valueType, Blockly.JavaScript.valueToCode(block, 'VALUE', Blockly.JavaScript.ORDER_ASSIGNMENT) || 0) * 1000;
            return "pause(" + argument0 + ");";
        }


        // transform note durations in ms
        for (var i = 0; i < durations.length; i++) {
            durations[i][1] = Math.round(((BPM / 60) * durations[i][1]) * 100).toString();
        }

        var notes = [
            ["C", "C"],
            ["C# / D♭", "C#"],
            ["D", "D"],
            ["D# / E♭", "D#"],
            ["E", "E"],
            ["F", "F"],
            ["F# / G♭", "F#"],
            ["G", "G"],
            ["G# / A♭", "G#"],
            ["A", "A"],
            ["A# / B♭", "A#"]
        ];



        var octaves = [
            // ["1", "1"],
            // ["2", "2"],
            ["3", "3"],
            ["4", "4"],
            ["5", "5"],
            ["6", "6"]
        ];


        // create note block
        Blockly.Blocks['play_note_duration_words'] = {
            init: function() {
                this.setColour = this.updateColour = setBlockColour.bind(this);
                this.setColour([249, 183, 87]);
                this.appendDummyInput()
                    .appendField("♫ ");

                this.appendDummyInput()
                    .appendField(new Blockly.FieldDropdown(notes), "note")
                    .appendField(new Blockly.FieldDropdown(octaves), "octave")
                    .appendField(new Blockly.FieldImage("img/clock.svg", 15, 15, "clock"))
                    .appendField(new Blockly.FieldDropdown(durations), "duration");
                this.setInputsInline(true);
                this.setPreviousStatement(true);
                this.setNextStatement(true);
                this.setTooltip('');
            }
        }



        Blockly.JavaScript['play_note_duration_words'] = function(block) {
            var dropdown_note = block.getFieldValue('note');
            var dropdown_octave = block.getFieldValue('octave');
            var dropdown_duration = block.getFieldValue('duration');
            return "playNote('" + dropdown_note + dropdown_octave + "'," + dropdown_duration + ");";
        }

        // code for multiple sensors
        //create sensors block
        // Blockly.Blocks['sensor'] = {
        //     init: function() {
        //         this.setColour = this.updateColour = setBlockColour.bind(this);
        //         this.setColour([105, 45, 85]);


        //         var input = this.appendDummyInput();


        //         input.appendField(new Blockly.FieldDropdown([
        //             ["A0", "A0"],
        //             ["A1", "A1"],
        //             ["A2", "A2"]
        //         ]), "sensorName");

        //         input.appendField(" ", "VALUE");

        //         // this.setInputsInline(true);
        //         this.setOutput(true, "Number");
        //         this.setTooltip('');
        //     },
        //     customUpdate: function(a) {
        //         this.setFieldValue(" " + a, 'VALUE');
        //     }
        // }
        // 

        // code for single sensor
        Blockly.Blocks['sensor'] = {
            init: function() {
                this.setColour = this.updateColour = setBlockColour.bind(this);
                this.setColour([105, 45, 85]);


                var input = this.appendDummyInput();
                input.appendField("Sensor value");
                input.appendField(" ", "VALUE");

                // this.setInputsInline(true);
                this.setOutput(true, "Number");
                this.setTooltip('');
            },
            customUpdate: function(a) {
                this.setFieldValue(" " + a, 'VALUE');
            }
        }


        Blockly.JavaScript['sensor'] = function(block) {
            var code = getSensorValue();
            // parseFloat(block.getFieldValue('VALUE'));
            return ["getSensorValue()", Blockly.JavaScript.ORDER_MEMBER];
        }




        function setBlockColour(rgb) {

            if (!rgb) return;
            var rgbLight = goog.color.lighten(rgb, 0.3);
            var rgbDark = goog.color.darken(rgb, 0.4);
            this.svgPathLight_.setAttribute('stroke', goog.color.rgbArrayToHex(rgbLight));
            this.svgPathDark_.setAttribute('fill', goog.color.rgbArrayToHex(rgbDark));
            this.svgPath_.setAttribute('fill', goog.color.rgbArrayToHex(rgb));

        }

        Blockly.JavaScript.STATEMENT_PREFIX = '\nhighlightBlock(%1);\n';
        Blockly.JavaScript.addReservedWords('highlightBlock');
    }

    function getSensorValue() {

        console.log(sensorValue)
        return sensorValue;

    }

    function playNote(n, d) {

        console.log("%c playing note", 'background: #222; color: #bada55', n.data, d.data);

        highlightPause = true;

        if (commandCallback) commandCallback({
            command: "N",
            data: {
                note: n.data,
                duration: d.data
            }
        })


        //finish the note
        //add 1000 ms because board has some delay
        currentTimeout = setTimeout(function() {

            if (commandCallback) commandCallback({
                command: "Z"
            });

            // currentTimeout = setTimeout(function() {
            highlightPause = false;
            stepCode();
            // }.bind(this), 1000);

        }.bind(this), d.data)
    }

    function pause(wait) {

        highlightPause = true;

        currentTimeout = setTimeout(function() {

            highlightPause = false;
            stepCode();

        }.bind(this), wait.data);

    }

    function sendCommand(c, wait) {

        console.log(commandCallback, c, wait.data)
        if (commandCallback) commandCallback({
            command: c
        })


        highlightPause = true;

        currentTimeout = setTimeout(function() {

            highlightPause = false;
            stepCode();

        }.bind(this), wait.data);
    }

    function onWorkspaceChanged(e) {

        if (changeCallback) changeCallback();

    }

    function stepCode() {

        var finished;
        try {
            finished = interpreter.step();
        } catch (e) {
            onCodeExecuted();
            console.log("error", e, e.stack);
        } finally {
            if (!finished) {
                onCodeExecuted();
                console.log("%c finished", 'background: #222; color: #bada55')
                return;
            }
        }


        if (!highlightPause) {
            stepCode();
        }
    }

    function onCodeExecuted() {
        // TODO handle errors and send them 


        if (completeCallback) completeCallback();
    }

    function styleDefaultBlocks() {

        // var resMultiplier = window.innerWidth > 1280 ? 1 : 0.8;
        // Blockly.BlockSvg.SEP_SPACE_X = resMultiplier * 15;
        // Blockly.BlockSvg.INLINE_PADDING_Y = resMultiplier * 5;
        // Blockly.BlockSvg.CORNER_RADIUS = resMultiplier * 4;
        // Blockly.BlockSvg.MIN_BLOCK_Y = resMultiplier * 40;
        // Blockly.BlockSvg.FIELD_HEIGHT = resMultiplier * 26;

        Blockly.Blocks['controls_repeat_ext'] = {

            init: function() {
                this.setHelpUrl(Blockly.Msg.CONTROLS_REPEAT_HELPURL);
                this.setColour(159);
                this.interpolateMsg(Blockly.Msg.CONTROLS_REPEAT_TITLE, ['TIMES', 'Number', Blockly.ALIGN_RIGHT],
                    Blockly.ALIGN_RIGHT);
                this.appendStatementInput('DO')
                    .appendField(Blockly.Msg.CONTROLS_REPEAT_INPUT_DO);
                this.setPreviousStatement(true);
                this.setNextStatement(true);
                this.setInputsInline(true);
                this.setTooltip(Blockly.Msg.CONTROLS_REPEAT_TOOLTIP);
            }
        }
    }

    function highlightBlock(id) {
        // console.log(id)
        Blockly.mainWorkspace.highlightBlock(id);
        highlightPause = true;

        currentTimeout = setTimeout(function() {

            highlightPause = false;
            stepCode();

        }.bind(this), 200);
    }

    function initApi(interpreter, scope) {
        var wrapper = function(a, w) {
            return interpreter.createPrimitive(sendCommand(a, w));
        };
        interpreter.setProperty(scope, 'sendCommand', interpreter.createNativeFunction(wrapper));

        var wrapper = function(a, d) {
            return interpreter.createPrimitive(playNote(a, d));
        };
        interpreter.setProperty(scope, 'playNote', interpreter.createNativeFunction(wrapper));

        var wrapper = function(a, w) {
            return interpreter.createPrimitive(pause(a));
        };
        interpreter.setProperty(scope, 'pause', interpreter.createNativeFunction(wrapper));

        var wrapper = function(a, w) {
            return interpreter.createPrimitive(getSensorValue());
        };
        interpreter.setProperty(scope, 'getSensorValue', interpreter.createNativeFunction(wrapper));


        var wrapper = function(id) {
            id = id ? id.toString() : '';
            return interpreter.createPrimitive(highlightBlock(id));
        };
        interpreter.setProperty(scope, 'highlightBlock', interpreter.createNativeFunction(wrapper));
    }


    BlocklyPanel = function(options) {

        initCubettoBlocks();
        styleDefaultBlocks();

        startCallback = options.onStart;
        changeCallback = options.onChange;
        commandCallback = options.onCommand;
        completeCallback = options.onComplete;

        this.workspace = Blockly.inject('blocklyDiv', {
            media: "js/media/",
            hasSounds: true,
            toolbox: document.getElementById('toolbox')
        });


        window.addEventListener('resize', this.onresize.bind(this), false);
        this.workspace.addChangeListener(onWorkspaceChanged.bind(this));


        this.onresize();
    }


    BlocklyPanel.prototype.onresize = function(e) {
        var blocklyArea = document.getElementById('blocklyArea');
        var blocklyDiv = document.getElementById('blocklyDiv');

        var element = blocklyArea;
        var x = 0;
        var y = 0;
        do {
            x += element.offsetLeft;
            y += element.offsetTop;
            element = element.offsetParent;
        } while (element);
        blocklyDiv.style.left = x + 'px';
        blocklyDiv.style.top = y + 'px';

        blocklyDiv.style.width = blocklyArea.offsetWidth + 'px';
        blocklyDiv.style.height = blocklyArea.offsetHeight + 'px';
    }


    BlocklyPanel.prototype.stopBlockly = function() {
        console.log("stopping code")
        highlightPause = true;
        clearInterval(currentTimeout);
    }

    BlocklyPanel.prototype.executeBlockly = function() {
        if (startCallback) startCallback();

        var code = Blockly.JavaScript.workspaceToCode();
        console.log(code)
        interpreter = new Interpreter(code, initApi);
        highlightPause = false;
        Blockly.mainWorkspace.traceOn(true);
        Blockly.mainWorkspace.highlightBlock(null);
        stepCode();
    }

    BlocklyPanel.prototype.updateBlocks = function(values) {

        // console.log(this)
        // Update blocks to show values.
        function updateSvgBlocks(blocks) {
            for (var i = 0, block; block = blocks[i]; i++) {
                if (block.type == "sensor") {

                    // code for multiple sensors
                    // block.customUpdate(values[block.getFieldValue("sensorName")]);

                    sensorValue = values["A5"];
                    // code for single sensor due to board limitations
                    block.customUpdate(values["A5"]);
                }
            }
        }

        updateSvgBlocks(this.workspace.getAllBlocks());
        // updateSvgBlocks(this.workspace.toolbox_.getAllBlocks());
    }


    return BlocklyPanel;

})();