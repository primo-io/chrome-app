SerialConnector = function(options) {

    if (options) {

        this.onAvailableCallback = options.onAvailable;
        this.onLostConnectionCallback = options.onLostConnection;
        this.onReceiveCallback = options.onReceive;

    }

    this.tempPorts = [];
    this.ports = [];
    this.serialValues = {};
    this.cubettoFound = false;

    if (chrome.serial) {

        chrome.serial.onReceive.addListener(this.onReceive.bind(this));
        chrome.serial.onReceiveError.addListener(this.onReceiveError.bind(this));

    }

    this.readPorts();

};



SerialConnector.prototype = {

    readPorts: function() {

        if (chrome.serial) {
            this.getPorts();
        }
    },

    getPorts: function() {

        if (!this.cubettoFound) {

            // console.log("reading ports");
            chrome.serial.getDevices(function(ports) {

                ports = ports.filter(function(a) {
                    return !a.path.match(/Bluetooth/);
                })

                if (ports && !this.compareArrays(ports, this.ports)) {


                    // console.log("resetting ports");

                    this.ports = ports.slice(0);
                    this.tempPorts = ports.slice(0);
                    this.nextPort();
                } else {
                    this.nextPort();
                }

            }.bind(this));
        }

        this.portCheckInterval = setTimeout(this.getPorts.bind(this), 4000);
    },

    compareArrays: function(array) {

        if (!array) return false;
        if (this.length != array.length) return false;

        for (var i = 0, l = this.length; i < l; i++) {
            if (this[i].port != array[i].port) return false;
        }
        return true;

    },


    nextPort: function() {

        if (this.tempPorts.length > 0) {
            this.selectedPort = this.tempPorts.pop().path;
            console.log("trying port" + this.selectedPort);

            chrome.serial.connect(this.selectedPort, {
                bitrate: 115200
            }, this.onOpen.bind(this));

        } else {
            if (this.onLostConnectionCallback) this.onLostConnectionCallback();
            console.log("all ports checked. no valid ports found");
        }
    },


    send: function(string) {

        // console.log("%string sending command ", 'background: red; color: white', string);
        chrome.serial.send(this.connectionId, this.convertStringToArrayBuffer(string + "\n"), this.onSend);

    },

    onOpen: function(openInfo) {

        if (openInfo) {

            console.log("open connection")
            this.connectionId = openInfo.connectionId;

            if (this.connectionId == -1) {

                this.cubettoFound = false;
                if (this.onLostConnectionCallback) this.onLostConnectionCallback();

                return;
            }


            // found a port
            this.cubettoFound = true;
            if (this.onAvailableCallback) this.onAvailableCallback(this.selectedPort);

            // make a tune
            chrome.serial.send(this.connectionId, this.convertStringToArrayBuffer("T\n"), this.onSend);


        }

    },

    onSend: function(info) {
        // console.log("sent", info)
    },

    onReceive: function(info) {

        if (info.connectionId == this.connectionId && info.data) {

            // read values from the serial
            var str = this.convertArrayBufferToString(info.data);

            var a = str.split("#");

            for (var i = 0; i < a.length; i++) {
                var s = a[i];
                this.serialValues[s.substr(0, 2)] = s.substr(2).trim();
            }

            if (this.onReceiveCallback) this.onReceiveCallback();

        } else {

            this.serialValues = {};
            this.onReceiveError();

        }

    },

    onReceiveError: function(info) {

        if (this.onLostConnectionCallback) this.onLostConnectionCallback();
        this.cubettoFound = false;

    },

    convertArrayBufferToString: function(buf) {
        var encodedString = String.fromCharCode.apply(null, new Uint8Array(buf));
        return decodeURIComponent(escape(encodedString))
    },

    convertStringToArrayBuffer: function(str) {
        var buf = new ArrayBuffer(str.length);
        var bufView = new Uint8Array(buf);
        for (var i = 0; i < str.length; i++) {
            bufView[i] = str.charCodeAt(i);
        }
        return buf;
    }

}