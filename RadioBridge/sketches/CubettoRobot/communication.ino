void checkRadio()
{
  bool tx, fail, rx;

  radio.whatHappened(tx, fail, rx);

  // Have we successfully transmitted?
  if (tx)
  {
    debugPrintf("Message/ACK sent\n");
    radio.startListening(); 
  }

  // Have we failed to transmit?
  if (fail)
  {
    debugPrintf("Transmission failed\n");
    radio.flush_tx();
    radio.startListening(); 
  }

  // Did we receive a message?
  if (rx || radio.available())
  {
    // Get the received message
    RadioMessage rxMsg;
    radio.read(&rxMsg, sizeof(rxMsg));

    debugMessage(rxMsg);

    // Check if the message is good, otherwise ignore it
    if (checkMessage(rxMsg))
    {
      if (rxMsg.code == PRIMO_SYSTEM_MESSAGE_CODE)
      {
        if (rxMsg.payload[0] == PRIMO_ACKNOWLEDGE_CODE)
          ackReceived = true;
      }
      else
      {
        // Check that no previous message processing is ongoing
        if (noMessageReceived)
        {
          commandMsg = rxMsg;
          noMessageReceived = false;
          sendAcknowledge(PRIMO_STATUS_OK);
        }
        else
          sendAcknowledge(PRIMO_STATUS_WARN_CMD_IGNORED);
      }
    }
  }
}

////////////////////////////////////////////////////////////////////////////////

void startTransmissionMode()
{
  radio.stopListening();

  for (uint8_t i = 0; i < 130; ++i)
    __asm__("nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t""nop\n\t");
}

////////////////////////////////////////////////////////////////////////////////

bool checkMessage (RadioMessage &msgToCheck)
{
  if ((msgToCheck.senderId != PRIMO_RADIO_BRIDGE_ID) || (msgToCheck.checksum != calcMessageChecksum(msgToCheck)))
    return false;

  if (sessionId == 0UL)
  {
    sessionId = msgToCheck.sessionId;
    debugPrintf("Got new session ID: %lu\n", sessionId);
  }
  else
  {
    if (sessionId != msgToCheck.sessionId)
      return false;
  }

  return true;
}

////////////////////////////////////////////////////////////////////////////////

void setMessageChecksum (RadioMessage &msg)
{
  msg.checksum = calcMessageChecksum(msg);
}

////////////////////////////////////////////////////////////////////////////////

uint8_t calcMessageChecksum (RadioMessage &msg)
{
  uint8_t checksumAccumulator = 0x00;
  uint8_t *msgPtr = (uint8_t *) &msg;

  for (int idx = 0; idx < sizeof(msg) - sizeof(msg.checksum); ++idx)
    checksumAccumulator += *(msgPtr++);

  return checksumAccumulator;
}

////////////////////////////////////////////////////////////////////////////////

void sendAcknowledge (uint8_t statusCode)
{
  RadioMessage ackMsg;
  ackMsg.senderId = PRIMO_CUBETTO_ROBOT_ID;
  ackMsg.sessionId = sessionId;
  ackMsg.code = PRIMO_SYSTEM_MESSAGE_CODE;
  ackMsg.payload[0] = PRIMO_ACKNOWLEDGE_CODE;
  *(uint32_t *)(ackMsg.payload + 1) = ++ackMessageCount;
  ackMsg.payload[5] = statusCode;

  for (int idx = 6; idx < PRIMO_MAX_MESSAGE_PAYLOAD_SIZE; ++idx)
    ackMsg.payload[idx] = ' ';

  setMessageChecksum(ackMsg);

  startTransmissionMode();
  radio.startWrite(&ackMsg, sizeof(ackMsg), 0);
}

////////////////////////////////////////////////////////////////////////////////

void sendResponse (uint8_t cmdCode, String returnValues)
{
  sendMessage(String((char) PRIMO_RESPONSE_FROM_COMMAND) + String((char) cmdCode) + returnValues);
}

////////////////////////////////////////////////////////////////////////////////

void sendMessage (String msg)
{
  if (msg.length() < 1)
    return;

  if (msg.length() > PRIMO_MAX_MESSAGE_SIZE)
    msg = msg.substring(0, PRIMO_MAX_MESSAGE_SIZE);

  while (millis() < nextTxTime);

  nextTxTime = millis() + PRIMO_MESSAGE_TX_INTERVAL;

  if (!ackReceived)
  {
    uint32_t ackRxTimeoutTime = millis() + PRIMO_ACKNOWLEDGE_RX_TIMEOUT;
  
    while (!ackReceived)
      if (millis() > ackRxTimeoutTime)
        break;
  }

  RadioMessage radioMsg;

  radioMsg.code = (uint8_t) msg.charAt(0);

  int idx;

  for (idx = 1; idx < msg.length(); ++idx)
    radioMsg.payload[idx - 1] = (uint8_t) msg.charAt(idx);

  for (; idx < PRIMO_MAX_MESSAGE_SIZE; ++idx)
    radioMsg.payload[idx - 1] = (uint8_t) ' ';

  radioMsg.senderId = PRIMO_CUBETTO_ROBOT_ID;
  radioMsg.sessionId = sessionId;

  setMessageChecksum(radioMsg);
  ackReceived = false;

  startTransmissionMode();
  radio.startWrite(&radioMsg, sizeof(radioMsg), 0);
}

////////////////////////////////////////////////////////////////////////////////

void dumpMessage (RadioMessage &msgToDump)
{
  bool gotAck = ((msgToDump.code == PRIMO_SYSTEM_MESSAGE_CODE) && (msgToDump.payload[0] == PRIMO_ACKNOWLEDGE_CODE));

  debugPrintf("Got %s\n", gotAck ? "ACK" : "message");

  debugPrintf("  Sender ID: %lu\n", msgToDump.senderId);
  debugPrintf("  Session ID: %lu\n", msgToDump.sessionId);

  if (gotAck)
  {
    debugPrintf("  Message counter: %lu\n", *(uint32_t *)(msgToDump.payload + 1));
    debugPrintf("  Status code: %02X\n", msgToDump.payload[5]);
  }
  else
  {
    debugPrintf("  Command code: %02X\n", msgToDump.code);
  
    debugPrintf("  Command arguments:");
    for (int cmdIdx = 0; cmdIdx < PRIMO_MAX_MESSAGE_PAYLOAD_SIZE; ++cmdIdx)
      debugPrintf(" %02X", msgToDump.payload[cmdIdx]);
    debugPrintf("\n");
  
    debugPrintf("  Command check: %s\n", (checkCommand(msgToDump) ? "OK" : "BAD!!!"));
  }

  debugPrintf("  Message check: %s\n", (checkMessage(msgToDump) ? "OK" : "BAD!!!"));
  debugPrintf("  Checksum: %02X\n", msgToDump.checksum);
}

