void filterInvalidInstructions (CommandsMessage &commandsMsg)
{
  int instrIdx = 0;
  
  while (instrIdx < PRIMO_MAX_MAIN_INSTRUCTIONS)
  {
    if (isKnownInstruction(commandsMsg.mainInstructions[instrIdx]))
      ++instrIdx;
    else
      break;
  }
  
  while (instrIdx < PRIMO_MAX_MAIN_INSTRUCTIONS)
    commandsMsg.mainInstructions[instrIdx++] = PRIMO_COMMAND_NONE;

  instrIdx = 0;
  
  while (instrIdx < PRIMO_MAX_FUNCTION_INSTRUCTIONS)
  {
    if (isKnownInstruction(commandsMsg.functionInstructions[instrIdx]))
      ++instrIdx;
    else
      break;
  }
  
  while (instrIdx < PRIMO_MAX_FUNCTION_INSTRUCTIONS)
    commandsMsg.functionInstructions[instrIdx++] = PRIMO_COMMAND_NONE;
}

////////////////////////////////////////////////////////////////////////////////

uint8_t swapBits (uint8_t value)
{
  // Swap bits 0 and 2 (bit 3 is preserved, all other bits are cleared)
  return ((value & 0x04) >> 2) | (value & 0x02) | ((value & 0x01) << 2);
}

////////////////////////////////////////////////////////////////////////////////

uint8_t rotateBits (uint8_t value)
{
  // Rotate the 3 LSBs (all other bits are cleared)
  return ((value & 0x06) >> 1) | ((value & 0x01) << 2);
}

////////////////////////////////////////////////////////////////////////////////

bool isKnownInstruction(uint8_t instrCode)
{
  switch (instrCode)
  {
    case PRIMO_COMMAND_RIGHT :
    case PRIMO_COMMAND_LEFT :
    case PRIMO_COMMAND_FORWARD :
    case PRIMO_COMMAND_FUNCTION :
      return true;

    default :
      return false;
  }
}

