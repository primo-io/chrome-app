World3D = function() {

    /* dom */
    this.el = document.querySelectorAll("#level")[0];


    var width = Math.round(this.el.offsetWidth);
    var height = Math.round(this.el.offsetHeight);


    this.scene = new THREE.Scene();
    this.container = new THREE.Object3D();

    this.pathCommands = [];

    /* renderer */
    this.renderer = new THREE.WebGLRenderer({
        autoClear: true,
        antialias: true,
        autoClearColor: 0xCC3333
    });

    this.renderer.setPixelRatio(window.devicePixelRatio);

    this.renderer.setClearColor(0xCC3333);

    this.renderer.setSize(width - 40, height - 40);
    this.renderer.shadowMapEnabled = true;
    this.renderer.shadowMapType = THREE.PCFSoftShadowMap;
    this.obj = new THREE.Mesh(new THREE.BoxGeometry(1, 1, .1), new THREE.MeshBasicMaterial({
        color: 0xFFFFFF
    }));

    this.obj.position.x = .5;
    this.obj.position.y = .5;
    // this.container.add(this.obj)
    this.worldScale = 6;

    /* camera */
    var r = height / width;
    this.camera = new THREE.OrthographicCamera(-this.worldScale, this.worldScale, this.worldScale * r, -this.worldScale * r, -10, 60);
    this.camera.position.x = 10;
    this.camera.position.y = 10;
    this.camera.position.z = 10;
    this.camera.lookAt(this.container.position);

    // quick fix to pan the camera so that the scene appears on the center
    // this.camera.position.y = 11;

    light = new THREE.AmbientLight(0xFFFFFF);
    // this.container.add(light);


    var material = new THREE.LineBasicMaterial({
        color: 0xFFFFFF
    });

    var size = 8;

    for (var i = 0; i <= size; i++) {
        var geometry = new THREE.Geometry();
        geometry.vertices.push(
            new THREE.Vector3(0, i, 0),
            new THREE.Vector3(size, i, 0)
        );
        var line = new THREE.Line(geometry, material);
        line.translateX(-size / 2);
        line.translateY(-size / 2);
        this.scene.add(line);
    }

    for (var i = 0; i <= size; i++) {
        var geometry = new THREE.Geometry();
        geometry.vertices.push(
            new THREE.Vector3(i, 0, 0),
            new THREE.Vector3(i, size, 0)
        );
        var line = new THREE.Line(geometry, material);
        line.translateX(-size / 2);
        line.translateY(-size / 2);
        this.scene.add(line);
    }




    // bigPlane.rotation.x = Math.PI * .5;
    this.scene.rotation.x = -Math.PI * .5;
    this.scene.add(this.container);



    // window.addEventListener('resize', this.onResize.bind(this), false);
    // this.onResize();


    // this.levelMesh.castShadow = true;
    // this.levelMesh.receiveShadow = true;
    this.el.appendChild(this.renderer.domElement);

    // this.offset = .5;
    // this.cubetto = new Cubetto.Cuby();
    // this.cubetto.offset = this.offset;

}
World3D.prototype.update = function() {
    // console.log("msg")
    this.renderer.render(this.scene, this.camera);
}

World3D.prototype.renderPath = function(blocks) {

    var commands = [];

    console.log("rendering")
    for (var i = 0; i < blocks.length; i++) {

        var b = blocks[i];
        switch (b.type) {

            case "left":
                commands.push("L");
                break;

            case "right":
                commands.push("R");
                break;

            case "forward":
                commands.push("F");
                break;

        }

    }

    var currentPlane;
    var ok = false;
    var go = false;

    if (commands.length == this.pathCommands.length) {

        for (var i = 0; i < this.pathCommands.length; i++) {
            if (this.pathCommands[i] != commands[i]) {
                go = true;
                break;
            }
        }

    } else {
        go = true;
    }
    console.log(commands, this.pathCommands, go)
    if (!go) return;

    var container = this.container;

    for (var i = container.children.length - 1; i >= 0; i--) {
        var obj = container.children[i];
        container.remove(obj);
    }

    //reset placeholder
    this.obj.position.x = .5;
    this.obj.position.y = .5;
    this.obj.rotation.x = 0;
    this.obj.rotation.y = 0;
    this.obj.rotation.z = 0;

    this.pathCommands = commands.slice(0);

    for (var i = 0; i < commands.length; i++) {
        switch (commands[i].toUpperCase()) {
            case "L":
                this.obj.rotateOnAxis(new THREE.Vector3(0, 0, 1), -Math.PI * .5);
                break;
            case "R":
                this.obj.rotateOnAxis(new THREE.Vector3(0, 0, 1), Math.PI * .5);
                break;
            case "F":
                this.obj.translateOnAxis(new THREE.Vector3(0, 1, 0), 1);
                ok = true;
                break;
        }
        this.obj.updateMatrix();

        if (ok) {
            var currentPlane = new THREE.Mesh(new THREE.BoxGeometry(1, 1, .1), new THREE.MeshBasicMaterial({
                color: 0xFFFFFF,
                opacity: .5,
                transparent: true
            }));
            currentPlane.applyMatrix(this.obj.matrix);

            this.container.add(currentPlane);
        }

    }

}