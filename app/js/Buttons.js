Buttons = function(window) {

    this.runBtn = document.getElementById("runbtn");
    this.testBtn = document.getElementById("testbtn");
    this.status = document.getElementById("status");

    if (!this.runBtn || !this.testBtn) return;

    this.canRun = false;
    this.enableRunButton(false);
    this.enableTestButton(false);

}



Buttons.prototype.enable = function() {
    this.canTest = true;
    this.enableTestButton(true);
    if (this.canRun) this.enableRunButton(true);
}

Buttons.prototype.disable = function() {
    this.canTest = false;
    this.enableTestButton(false);
    this.enableRunButton(false);
}

Buttons.prototype.enableRunButton = function(b) {
    this[!b ? "addClass" : "removeClass"](this.runBtn, "disabled");
}

Buttons.prototype.enableTestButton = function(b) {
    this[!b ? "addClass" : "removeClass"](this.testBtn, "disabled");
}


Buttons.prototype.setButtonLabelToStop = function(b, button) {

    console.log("setButtonLabelToStop", b, button);

    if (!button) return;

    if (b) {
        button.querySelector("label").textContent = "STOP";
    } else {
        button.querySelector("label").textContent = button == this.testBtn ? "TEST" : "RUN";
    }
}

Buttons.prototype.setAvailable = function(b, msg) {
    this.canRun = b;
    if (b && this.canTest) {
        this.enableRunButton(true);
    } else {
        this.enableRunButton(false);
    }
}



Buttons.prototype.addClass = function(element, classToAdd) {
    var currentClassValue = element.className;

    if (currentClassValue.indexOf(classToAdd) == -1) {
        if ((currentClassValue == null) || (currentClassValue === "")) {
            element.className = classToAdd;
        } else {
            element.className += " " + classToAdd;
        }
    }
}

Buttons.prototype.removeClass = function(element, classToRemove) {
    var currentClassValue = element.className;

    if (currentClassValue == classToRemove) {
        element.className = "";
        return;
    }

    var classValues = currentClassValue.split(" ");
    var filteredList = [];

    for (var i = 0; i < classValues.length; i++) {
        if (classToRemove != classValues[i]) {
            filteredList.push(classValues[i]);
        }
    }

    element.className = filteredList.join(" ");
}