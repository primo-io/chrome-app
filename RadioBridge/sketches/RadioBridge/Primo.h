// Primo version number
#define PRIMO_CUBETTO_PLAYSET_VERSION "1.2"

////////////////////////////////////////////////////////////////////////////////

// Command codes
#define PRIMO_COMMAND_NONE                 ((uint8_t) ' ')
#define PRIMO_COMMAND_PLAY_NOTE            ((uint8_t) 'N')
#define PRIMO_COMMAND_MUTE                 ((uint8_t) 'Z')
#define PRIMO_COMMAND_WRITE_ACTUATOR_VALUE ((uint8_t) 'W')
#define PRIMO_COMMAND_SPIN_MOTOR_CLOCKWISE ((uint8_t) 'M')
#define PRIMO_COMMAND_MOVE_FORWARD         ((uint8_t) 'F')
#define PRIMO_COMMAND_MOVE_BACKWARDS       ((uint8_t) 'B')
#define PRIMO_COMMAND_ROTATE_RIGHT         ((uint8_t) 'R')
#define PRIMO_COMMAND_ROTATE_LEFT          ((uint8_t) 'L')
#define PRIMO_COMMAND_GET_SENSORS_READINGS ((uint8_t) 'S')
#define PRIMO_COMMAND_PLAY_CONNECTED_TUNE  ((uint8_t) 'T')

////////////////////////////////////////////////////////////////////////////////

// Response codes
#define PRIMO_RESPONSE_FROM_COMMAND ((uint8_t) 'E')

////////////////////////////////////////////////////////////////////////////////

// System message codes
#define PRIMO_SYSTEM_MESSAGE_CODE ((uint8_t) '+')
#define PRIMO_ACKNOWLEDGE_CODE    ((uint8_t) 'A')

////////////////////////////////////////////////////////////////////////////////

// Status codes
#define PRIMO_STATUS_OK               ((uint8_t) 0)
#define PRIMO_STATUS_WARN_CMD_IGNORED ((uint8_t) 1)

////////////////////////////////////////////////////////////////////////////////

// This 32-bit value is the identifier for ANY Cubetto Playset.  It is inserted
// into every radio packet sent by ANY Radio Bridge.  A check is made on every
// radio packet, received by ANY Cubetto Robot, that this value is present.
#define PRIMO_RADIO_BRIDGE_ID 0xDE1D875AUL

// This 32-bit value is the identifier for ANY Cubetto Playset.  It is inserted
// into every radio packet sent by ANY Cubetto Robot.  A check is made on every
// radio packet, received by ANY Interface, that this value is present.
#define PRIMO_CUBETTO_ROBOT_ID 0xDE1D875BUL

// Size of the message code
#define PRIMO_MESSAGE_CODE_SIZE 1

// Maximum size of a message payload in bytes
#define PRIMO_MAX_MESSAGE_PAYLOAD_SIZE 22

// Maximum size of a message (code + payload) in bytes
#define PRIMO_MAX_MESSAGE_SIZE (PRIMO_MESSAGE_CODE_SIZE + PRIMO_MAX_MESSAGE_PAYLOAD_SIZE)

// Maximum size of a response in bytes
#define PRIMO_MAX_RESPONSE_SIZE (PRIMO_MAX_MESSAGE_SIZE - PRIMO_MESSAGE_CODE_SIZE)

typedef struct radioMessage
{
  uint32_t senderId;
  uint32_t sessionId;
  uint8_t code;
  uint8_t payload[PRIMO_MAX_MESSAGE_PAYLOAD_SIZE];
  uint8_t checksum;
}
RadioMessage;

uint32_t sessionId = 0UL;
uint32_t ackMessageCount = 0UL;
uint32_t nextTxTime = 0UL;

#define PRIMO_ACKNOWLEDGE_RX_TIMEOUT 500UL
#define PRIMO_MESSAGE_TX_INTERVAL    10UL

// Communication pipes addresses
uint8_t PRIMO_RADIOBRIDGE2CUBETTO_PIPE_ADDRESS[5] = { 0xE8,0xE8,0xF0,0xF0,0xE3 };
uint8_t PRIMO_CUBETTO2RADIOBRIDGE_PIPE_ADDRESS[5] = { 0xE8,0xE8,0xF0,0xF0,0xE4 };

