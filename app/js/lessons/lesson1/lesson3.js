var world;


Lesson3 = function() {
    Lesson.call(this);

    world = new World3D();

    this.animate();
}




Lesson3.prototype = Object.create(Lesson.prototype);
Lesson3.prototype.constructor = Lesson3;

Lesson3.prototype.onBlocklyChanged = function() {

    Lesson.prototype.onBlocklyChanged.call(this);

    var commands = [];

    var blocks = this.panel.workspace.getAllBlocks();
    if (blocks.length) {
        console.log("onBlocklyChanged");
        world.renderPath(blocks);
    }
}

Lesson3.prototype.animate = function() {

    world.update();

    requestAnimationFrame(this.animate.bind(this));

}

var l = new Lesson3();