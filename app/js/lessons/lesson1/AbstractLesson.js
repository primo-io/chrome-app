Lesson = function() {



    this.serialMode = false;
    this.cubettoAvailable = false;

    this.panel = new BlocklyPanel({
        onStart: this.onBlocklyStarted.bind(this),
        onChange: this.onBlocklyChanged.bind(this),
        onCommand: this.onBlocklyCommand.bind(this),
        onComplete: this.onBlocklyExecuted.bind(this)
    });

    window.addEventListener('message', this.onMessageReceived.bind(this));

}



Lesson.prototype.onMessageReceived = function(event) {

    // console.log("onMessageReceived", event.data)
    this.eventsource = event.source;
    this.eventorigin = event.origin;

    if (event.data && event.data.command) {

        switch (event.data.command) {
            case "start":
                this.sendMessage({
                    data: "ok"
                });
                break;

            case 'stop':
                this.stop();
                break;

            case 'available':
                this.cubettoAvailable = true;
                break;

            case 'lostConnection':
                this.cubettoAvailable = false;
                break;

            case "test":
                this.serialMode = false;
                this.panel.executeBlockly();
                break;

            case "run":
                this.serialMode = true;
                this.panel.executeBlockly();
                break;
            case 'serial':
                break;

        }
    }
}



Lesson.prototype.onBlocklyChanged = function() {

    var blocks = this.panel.workspace.getAllBlocks();
    this.sendMessage({
        command: blocks.length > 0 ? "enableButtons" : "disableButtons"
    });
}


Lesson.prototype.onBlocklyExecuted = function(command) {

    this.sendMessage({
        command: "finish"
    })

}


Lesson.prototype.onBlocklyStarted = function(event) {
    //to override

}

Lesson.prototype.stop = function(event) {
    this.panel.stopBlockly();
}

Lesson.prototype.sendMessage = function(data) {
    if (!data || !this.eventorigin || !this.eventsource) {
        console.log("couldn't send message", data, this.eventsource, this.eventorigin);
        return;
    } else {
        this.eventsource.postMessage(data, this.eventorigin);
    }
}

//used only to play sounds in test mode
Lesson.prototype.playSound = function(data) {
    this.sendMessage({
        command: 'sound',
        data: data
    });
}

Lesson.prototype.onBlocklyCommand = function(c) {

    if (this.serialMode) {

        console.log("%c sending command ", 'background: red; color: white', c.command.data);

        this.sendMessage({
            command: 'serial',
            data: c.command.data
        });

    }

}