#define PRIMO_BUZZER_PIN 13

////////////////////////////////////////////////////////////////////////////////

// Notes
#define PRIMO_NOTE_B0  31
#define PRIMO_NOTE_C1  33
#define PRIMO_NOTE_CS1 35
#define PRIMO_NOTE_D1  37
#define PRIMO_NOTE_DS1 39
#define PRIMO_NOTE_E1  41
#define PRIMO_NOTE_F1  44
#define PRIMO_NOTE_FS1 46
#define PRIMO_NOTE_G1  49
#define PRIMO_NOTE_GS1 52
#define PRIMO_NOTE_A1  55
#define PRIMO_NOTE_AS1 58
#define PRIMO_NOTE_B1  62
#define PRIMO_NOTE_C2  65
#define PRIMO_NOTE_CS2 69
#define PRIMO_NOTE_D2  73
#define PRIMO_NOTE_DS2 78
#define PRIMO_NOTE_E2  82
#define PRIMO_NOTE_F2  87
#define PRIMO_NOTE_FS2 93
#define PRIMO_NOTE_G2  98
#define PRIMO_NOTE_GS2 104
#define PRIMO_NOTE_A2  110
#define PRIMO_NOTE_AS2 117
#define PRIMO_NOTE_B2  123
#define PRIMO_NOTE_C3  131
#define PRIMO_NOTE_CS3 139
#define PRIMO_NOTE_D3  147
#define PRIMO_NOTE_DS3 156
#define PRIMO_NOTE_E3  165
#define PRIMO_NOTE_F3  175
#define PRIMO_NOTE_FS3 185
#define PRIMO_NOTE_G3  196
#define PRIMO_NOTE_GS3 208
#define PRIMO_NOTE_A3  220
#define PRIMO_NOTE_AS3 233
#define PRIMO_NOTE_B3  247
#define PRIMO_NOTE_C4  262
#define PRIMO_NOTE_CS4 277
#define PRIMO_NOTE_D4  294
#define PRIMO_NOTE_DS4 311
#define PRIMO_NOTE_E4  330
#define PRIMO_NOTE_F4  349
#define PRIMO_NOTE_FS4 370
#define PRIMO_NOTE_G4  392
#define PRIMO_NOTE_GS4 415
#define PRIMO_NOTE_A4  440
#define PRIMO_NOTE_AS4 466
#define PRIMO_NOTE_B4  494
#define PRIMO_NOTE_C5  523
#define PRIMO_NOTE_CS5 554
#define PRIMO_NOTE_D5  587
#define PRIMO_NOTE_DS5 622
#define PRIMO_NOTE_E5  659
#define PRIMO_NOTE_F5  698
#define PRIMO_NOTE_FS5 740
#define PRIMO_NOTE_G5  784
#define PRIMO_NOTE_GS5 831
#define PRIMO_NOTE_A5  880
#define PRIMO_NOTE_AS5 932
#define PRIMO_NOTE_B5  988
#define PRIMO_NOTE_C6  1047
#define PRIMO_NOTE_CS6 1109
#define PRIMO_NOTE_D6  1175
#define PRIMO_NOTE_DS6 1245
#define PRIMO_NOTE_E6  1319
#define PRIMO_NOTE_F6  1397
#define PRIMO_NOTE_FS6 1480
#define PRIMO_NOTE_G6  1568
#define PRIMO_NOTE_GS6 1661
#define PRIMO_NOTE_A6  1760
#define PRIMO_NOTE_AS6 1865
#define PRIMO_NOTE_B6  1976
#define PRIMO_NOTE_C7  2093
#define PRIMO_NOTE_CS7 2217
#define PRIMO_NOTE_D7  2349
#define PRIMO_NOTE_DS7 2489
#define PRIMO_NOTE_E7  2637
#define PRIMO_NOTE_F7  2794
#define PRIMO_NOTE_FS7 2960
#define PRIMO_NOTE_G7  3136
#define PRIMO_NOTE_GS7 3322
#define PRIMO_NOTE_A7  3520
#define PRIMO_NOTE_AS7 3729
#define PRIMO_NOTE_B7  3951
#define PRIMO_NOTE_C8  4186
#define PRIMO_NOTE_CS8 4435
#define PRIMO_NOTE_D8  4699
#define PRIMO_NOTE_DS8 4978

////////////////////////////////////////////////////////////////////////////////

void playPowerOnTune()
{
  tone(PRIMO_BUZZER_PIN, PRIMO_NOTE_C6);
  delay(375);
  tone(PRIMO_BUZZER_PIN, PRIMO_NOTE_E6);
  delay(125);
  tone(PRIMO_BUZZER_PIN, PRIMO_NOTE_G6);
  delay(250);
  tone(PRIMO_BUZZER_PIN, PRIMO_NOTE_E6);
  delay(125);
  noTone(PRIMO_BUZZER_PIN);
}

////////////////////////////////////////////////////////////////////////////////

void playConnectedTune()
{
  tone(PRIMO_BUZZER_PIN, PRIMO_NOTE_G6);
  delay(100);
  tone(PRIMO_BUZZER_PIN, PRIMO_NOTE_B6);
  delay(100);
  tone(PRIMO_BUZZER_PIN, PRIMO_NOTE_C7);
  delay(100);
  noTone(PRIMO_BUZZER_PIN);
}


////////////////////////////////////////////////////////////////////////////////

void playHappyTune()
{
  tone(PRIMO_BUZZER_PIN, PRIMO_NOTE_C6);
  delay(150);
  tone(PRIMO_BUZZER_PIN, PRIMO_NOTE_E6);
  delay(150);
  tone(PRIMO_BUZZER_PIN, PRIMO_NOTE_G6);
  delay(100);
  noTone(PRIMO_BUZZER_PIN);
}

////////////////////////////////////////////////////////////////////////////////

void playSadTune()
{
  tone(PRIMO_BUZZER_PIN, PRIMO_NOTE_G5);
  delay(150);
  tone(PRIMO_BUZZER_PIN, PRIMO_NOTE_DS5);
  delay(150);
  tone(PRIMO_BUZZER_PIN, PRIMO_NOTE_C5);
  delay(300);
  noTone(PRIMO_BUZZER_PIN);
}

////////////////////////////////////////////////////////////////////////////////
//
// TODO this could be done better.


void playNote(unsigned int i) {
  tone(PRIMO_BUZZER_PIN, i);
  Serial.println(i);
  
}


void playNoteFromString(const char *a) {
  Serial.println(a);
   if (strcmp(a, "B0") == 0) {
  	playNote(31);
  } else if (strcmp(a, "C1") == 0) {
  	playNote(33);
  } else if (strcmp(a, "CS1") == 0) {
  	playNote(35);
  } else if (strcmp(a, "D1") == 0) {
  	playNote(37);
  } else if (strcmp(a, "DS1") == 0) {
  	playNote(39);
  } else if (strcmp(a, "E1") == 0) {
  	playNote(41);
  } else if (strcmp(a, "F1") == 0) {
  	playNote(44);
  } else if (strcmp(a, "FS1") == 0) {
  	playNote(46);
  } else if (strcmp(a, "G1") == 0) {
  	playNote(49);
  } else if (strcmp(a, "GS1") == 0) {
  	playNote(52);
  } else if (strcmp(a, "A1") == 0) {
  	playNote(55);
  } else if (strcmp(a, "AS1") == 0) {
  	playNote(58);
  } else if (strcmp(a, "B1") == 0) {
  	playNote(62);
  } else if (strcmp(a, "C2") == 0) {
  	playNote(65);
  } else if (strcmp(a, "CS2") == 0) {
  	playNote(69);
  } else if (strcmp(a, "D2") == 0) {
  	playNote(73);
  } else if (strcmp(a, "DS2") == 0) {
  	playNote(78);
  } else if (strcmp(a, "E2") == 0) {
  	playNote(82);
  } else if (strcmp(a, "F2") == 0) {
  	playNote(87);
  } else if (strcmp(a, "FS2") == 0) {
  	playNote(93);
  } else if (strcmp(a, "G2") == 0) {
  	playNote(98);
  } else if (strcmp(a, "GS2") == 0) {
  	playNote(104);
  } else if (strcmp(a, "A2") == 0) {
  	playNote(110);
  } else if (strcmp(a, "AS2") == 0) {
  	playNote(117);
  } else if (strcmp(a, "B2") == 0) {
  	playNote(123);
  } else if (strcmp(a, "C3") == 0) {
  	playNote(131);
  } else if (strcmp(a, "CS3") == 0) {
  	playNote(139);
  } else if (strcmp(a, "D3") == 0) {
  	playNote(147);
  } else if (strcmp(a, "DS3") == 0) {
  	playNote(156);
  } else if (strcmp(a, "E3") == 0) {
  	playNote(165);
  } else if (strcmp(a, "F3") == 0) {
  	playNote(175);
  } else if (strcmp(a, "FS3") == 0) {
  	playNote(185);
  } else if (strcmp(a, "G3") == 0) {
  	playNote(196);
  } else if (strcmp(a, "GS3") == 0) {
  	playNote(208);
  } else if (strcmp(a, "A3") == 0) {
  	playNote(220);
  } else if (strcmp(a, "AS3") == 0) {
  	playNote(233);
  } else if (strcmp(a, "B3") == 0) {
  	playNote(247);
  } else if (strcmp(a, "C4") == 0) {
  	playNote(262);
  } else if (strcmp(a, "CS4") == 0) {
  	playNote(277);
  } else if (strcmp(a, "D4") == 0) {
  	playNote(294);
  } else if (strcmp(a, "DS4") == 0) {
  	playNote(311);
  } else if (strcmp(a, "E4") == 0) {
  	playNote(330);
  } else if (strcmp(a, "F4") == 0) {
  	playNote(349);
  } else if (strcmp(a, "FS4") == 0) {
  	playNote(370);
  } else if (strcmp(a, "G4") == 0) {
  	playNote(392);
  } else if (strcmp(a, "GS4") == 0) {
  	playNote(415);
  } else if (strcmp(a, "A4") == 0) {
  	playNote(440);
  } else if (strcmp(a, "AS4") == 0) {
  	playNote(466);
  } else if (strcmp(a, "B4") == 0) {
  	playNote(494);
  } else if (strcmp(a, "C5") == 0) {
  	playNote(523);
  } else if (strcmp(a, "CS5") == 0) {
  	playNote(554);
  } else if (strcmp(a, "D5") == 0) {
  	playNote(587);
  } else if (strcmp(a, "DS5") == 0) {
  	playNote(622);
  } else if (strcmp(a, "E5") == 0) {
  	playNote(659);
  } else if (strcmp(a, "F5") == 0) {
  	playNote(698);
  } else if (strcmp(a, "FS5") == 0) {
  	playNote(740);
  } else if (strcmp(a, "G5") == 0) {
  	playNote(784);
  } else if (strcmp(a, "GS5") == 0) {
  	playNote(831);
  } else if (strcmp(a, "A5") == 0) {
  	playNote(880);
  } else if (strcmp(a, "AS5") == 0) {
  	playNote(932);
  } else if (strcmp(a, "B5") == 0) {
  	playNote(988);
  } else if (strcmp(a, "C6") == 0) {
  	playNote(1047);
  } else if (strcmp(a, "CS6") == 0) {
  	playNote(1109);
  } else if (strcmp(a, "D6") == 0) {
  	playNote(1175);
  } else if (strcmp(a, "DS6") == 0) {
  	playNote(1245);
  } else if (strcmp(a, "E6") == 0) {
  	playNote(1319);
  } else if (strcmp(a, "F6") == 0) {
  	playNote(1397);
  } else if (strcmp(a, "FS6") == 0) {
  	playNote(1480);
  } else if (strcmp(a, "G6") == 0) {
  	playNote(1568);
  } else if (strcmp(a, "GS6") == 0) {
  	playNote(1661);
  } else if (strcmp(a, "A6") == 0) {
  	playNote(1760);
  } else if (strcmp(a, "AS6") == 0) {
  	playNote(1865);
  } else if (strcmp(a, "B6") == 0) {
  	playNote(1976);
  } else if (strcmp(a, "C7") == 0) {
  	playNote(2093);
  } else if (strcmp(a, "CS7") == 0) {
  	playNote(2217);
  } else if (strcmp(a, "D7") == 0) {
  	playNote(2349);
  } else if (strcmp(a, "DS7") == 0) {
  	playNote(2489);
  } else if (strcmp(a, "E7") == 0) {
  	playNote(2637);
  } else if (strcmp(a, "F7") == 0) {
  	playNote(2794);
  } else if (strcmp(a, "FS7") == 0) {
  	playNote(2960);
  } else if (strcmp(a, "G7") == 0) {
  	playNote(3136);
  } else if (strcmp(a, "GS7") == 0) {
  	playNote(3322);
  } else if (strcmp(a, "A7") == 0) {
  	playNote(3520);
  } else if (strcmp(a, "AS7") == 0) {
  	playNote(3729);
  } else if (strcmp(a, "B7") == 0) {
  	playNote(3951);
  } else if (strcmp(a, "C8") == 0) {
  	playNote(4186);
  } else if (strcmp(a, "CS8") == 0) {
  	playNote(4435);
  } else if (strcmp(a, "D8") == 0) {
  	playNote(4699);
  } else if (strcmp(a, "DS8") == 0) {
  	playNote(4978);
  }
}



