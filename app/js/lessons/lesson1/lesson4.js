var v = 0;

Lesson4 = function() {
    Lesson.call(this);


    this.fullyLoaded = false;
    this.slider = document.body.querySelector("#slider input")
    this.ctx = document.body.querySelector('#worldcontainer').getContext('2d');

    this.askedForValues = false;


    // this.cube = new RotatingCube();
    this.draw();
    // window.onmousemove = this.onMouseMove.bind(this);

}

Lesson4.prototype = Object.create(Lesson.prototype);
Lesson4.prototype.constructor = Lesson4;



Lesson4.prototype.onBlocklyCommand = function(c) {

    // console.log(c)
    //if it's a note command
    if (c.command == "N") {

        if (this.serialMode) {

            this.sendMessage({
                command: 'serial',
                data: "N" + c.data.note
            });

        } else {

            this.playSound({
                name: 'note' + c.data.note,
                duration: c.data.duration
            });

        }

    } else {
        if (this.serialMode) {

            this.sendMessage({
                command: 'serial',
                data: c.command
            });

        }
    }




}

Lesson4.prototype.onMessageReceived = function(event) {

    Lesson.prototype.onMessageReceived.call(this, event);

    if (!this.askedForValues) {
        this.askForSerialValues();
    }

    if (event.data && event.data.command) {

        if (event.data.command == "serial") {
            v = event.data.data;
            this.cubettoAvailable = true;

        }

        if (event.data.command == "available") {

            // lost connection, ask for values once connected
            this.askedForValues = false;
        }

        if (event.data.command == "available") {
            this.askForSerialValues();
        }


    }
}

Lesson4.prototype.askForSerialValues = function() {

    console.log("askForSerialValues")
    this.sendMessage({
        command: "serial",
        data: "S1"
    });

    this.askedForValues = true;

}

Lesson4.prototype.onMouseMove = function(e) {
    this.cube.cubetto.cube.rotation.y = ((e.clientX / window.innerWidth) * 2 - 1) * (Math.PI * .2);
    this.cube.cubetto.cube.rotation.z = -((e.clientY / window.innerHeight) * 2 - 1) * (Math.PI * .2);
}

Lesson4.prototype.draw = function() {

    // disable slider and update it with sensor values
    if (this.cubettoAvailable) {
        this.enableSlider(false);
        this.slider.value = v["A5"];
    } else {
        this.enableSlider(true);
    }

    this.panel.updateBlocks({
        "A5": this.slider.value
    });
    // this.cube.update();
    this.ctx.fillStyle = "hsl(200,0%, " + Math.round(this.slider.value / 5) + "%)";
    this.ctx.rect(0, 0, 1000, 1000);
    this.ctx.fill();
    // 

    requestAnimationFrame(this.draw.bind(this));

}

Lesson4.prototype.enableSlider = function(b) {

    if (this.sliderEnabled != b) {

        this.sliderEnabled = b;

        if (b) {
            this.slider.className = "";
        } else {
            this.slider.className = "disabled";
        }

    }

}

var lesson;
window.onload = function() {
    lesson = new Lesson4();

}