# Cubetto Chrome App

Chrome app to command Cubetto using Blockly via serial port.

The App currently works with Cubetto Board 2.0 and requires the board to be flashed with the included files.

Due to the app restrictions Blockly is loaded via an iframe (sandbox.html) 


## Chrome App Installation

Go to Chrome's Tools -> Extensions then click "Load unpacked extension...". -> Choose the folder called "app" -> click on Launch to get it working.

## API

| Name | Description | Parameter |
| ------------- | ----------- | ----------- |
| Nx\[x\]y	| Play note	| x\[x\]: note, y: octave
| Z	| Mute	| silence the buzzer
| Wxyyyy	| Write actuator value	| x: pin number, yyyy: value
| Mxayyyy	| Move motor clockwise	| x: motor to move (can be either "L" for left or "R" for right), a: sign (can be + or -), yyyy: number of steps to move
| Fxxxx | Move Cubetto forward | xxxx: steps |
| Bxxxx | Move Cubetto backwards | xxxx: steps |
| Rxxxx | Rotate Cubetto  right | xxxx: steps |
| Lxxxx | Rotate Cubetto  left | xxxx: steps |
| S	| Get sensors readings (returns #A0xxxx#A1xxxx#A2xxxx where xxxx is the read value)
 
