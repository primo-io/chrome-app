var rightDeg = 0;
var leftDeg = 0;
var targetLeftDeg = 0;
var targetRightDeg = 0;

Lesson1 = function() {
    Lesson.call(this);


    this.fullyLoaded = false;
    this.ctx = document.getElementById('canvas').getContext('2d');
    this.halfWheelWidth = (544 / 2) / 2;

    this.loadWheel();


}

Lesson1.prototype = Object.create(Lesson.prototype);
Lesson1.prototype.constructor = Lesson1;


Lesson1.prototype.onBlocklyCommand = function(c) {

    console.log("onBlocklyCommand", c)
    Lesson.prototype.onBlocklyCommand.call(this, c);


    if (!this.serialMode) {
        this.moveWheels(c.command.data);
    }
}

Lesson1.prototype.stop = function() {

    Lesson.prototype.stop.call(this);

    //reset 
    TweenLite.killTweensOf(window);
    targetLeftDeg = 0;
    targetRightDeg = 0;
    TweenLite.to(window, .25, {
        leftDeg: targetLeftDeg,
        rightDeg: targetRightDeg,
        ease: Sine.easeInOut
    })

}

Lesson1.prototype.moveWheels = function(string) {

    this.playSound({
        name: 'motor',
        duration: 5000
    });

    var command = string.substring(0, 1);
    var deg = (parseInt(string.substring(1)) * 0.07692) / 180 * Math.PI;

    targetLeftDeg = leftDeg;
    targetRightDeg = rightDeg;

    switch (command) {
        case "R":
            targetLeftDeg += deg;
            targetRightDeg += deg;


            // makeSound(4000, 50);
            break;
        case "F":
            targetLeftDeg -= deg;
            targetRightDeg += deg;

            // makeSound(3000, 50);
            break;
        case "L":
            targetLeftDeg -= deg;
            targetRightDeg -= deg;
            // makeSound(5000, 50);
            break;
        case "B":

            targetLeftDeg += deg;
            targetRightDeg -= deg;
            // makeSound(3000, 50);
            break;
    }


    console.log(window.targetLeftDeg)
    TweenLite.to(window, 5, {
        leftDeg: targetLeftDeg,
        rightDeg: targetRightDeg,
        ease: Sine.easeInOut
    })
}

Lesson1.prototype.loadWheel = function() {
    this.wheel = new Image();
    this.wheel.addEventListener("load", this.loadBg.bind(this), false);
    this.wheel.src = 'images/lesson1/wheel.png';
}

Lesson1.prototype.loadBg = function() {
    this.wheelbg = new Image();
    this.wheelbg.addEventListener("load", this.init.bind(this), false);
    this.wheelbg.src = 'images/lesson1/wheelbg.png';
}

Lesson1.prototype.init = function() {
    this.fullyLoaded = true;
    this.draw();
}

Lesson1.prototype.draw = function() {

    this.ctx.drawImage(this.wheelbg, 0, 0, 472, 472);
    this.ctx.save();
    this.ctx.translate(270 / 2, 485 / 2);
    this.ctx.rotate(rightDeg);
    this.ctx.drawImage(this.wheel, -this.halfWheelWidth, -this.halfWheelWidth, 272, 272);
    this.ctx.restore();
    this.ctx.save();
    this.ctx.translate(690 / 2, 485 / 2);
    this.ctx.rotate(leftDeg);
    this.ctx.drawImage(this.wheel, -this.halfWheelWidth, -this.halfWheelWidth, 272, 272);
    this.ctx.restore();

    requestAnimationFrame(this.draw.bind(this));

}


var lesson = new Lesson1();