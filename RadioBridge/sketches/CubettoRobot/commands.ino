bool checkCommand(RadioMessage &cmdMsg)
{
  return executeCommand(cmdMsg, true);
}

////////////////////////////////////////////////////////////////////////////////

bool executeCommand (RadioMessage &cmdMsg)
{
  return executeCommand(cmdMsg, false);
}

////////////////////////////////////////////////////////////////////////////////

bool executeCommand (RadioMessage &cmdMsg, bool dryRun)
{
  if (!dryRun)
    commandReturnValue = "";

  switch (cmdMsg.code)
  {
    case PRIMO_COMMAND_NONE :
      return true;

    case PRIMO_COMMAND_PLAY_NOTE :
      return playNote(getCommandArguments(cmdMsg), dryRun);

    case PRIMO_COMMAND_MUTE :
      return muteBuzzer(dryRun);

    case PRIMO_COMMAND_WRITE_ACTUATOR_VALUE :
      // Not implemented yet
      return false;

    case PRIMO_COMMAND_SPIN_MOTOR_CLOCKWISE :
      return spinMotorClockwise(getCommandArguments(cmdMsg), dryRun);

    case PRIMO_COMMAND_MOVE_FORWARD :
      return moveForward(getCommandArguments(cmdMsg), dryRun);

    case PRIMO_COMMAND_MOVE_BACKWARDS :
      return moveBackwards(getCommandArguments(cmdMsg), dryRun);

    case PRIMO_COMMAND_ROTATE_RIGHT :
      return rotateRight(getCommandArguments(cmdMsg), dryRun);

    case PRIMO_COMMAND_ROTATE_LEFT :
      return rotateLeft(getCommandArguments(cmdMsg), dryRun);

    case PRIMO_COMMAND_GET_SENSORS_READINGS :
      return getSensorsReadings(dryRun);

    case PRIMO_COMMAND_PLAY_CONNECTED_TUNE :
      return playConnectedTune(dryRun);
  }

  return false;
}

////////////////////////////////////////////////////////////////////////////////

String getCommandArguments (RadioMessage &cmdMsg)
{
  char cmdArgsBuffer[PRIMO_MAX_MESSAGE_PAYLOAD_SIZE + 1];
  
  memcpy(cmdArgsBuffer, cmdMsg.payload, PRIMO_MAX_MESSAGE_PAYLOAD_SIZE);
  cmdArgsBuffer[PRIMO_MAX_MESSAGE_PAYLOAD_SIZE] = '\0';

  String cmdArgs(cmdArgsBuffer);
  cmdArgs.trim();
  return cmdArgs;
}

////////////////////////////////////////////////////////////////////////////////

bool playNote (String cmdArgs, bool dryRun)
{
  unsigned int frequency = 0;

  if (cmdArgs.equals("B0"))
    frequency = PRIMO_NOTE_B0;

  else if (cmdArgs.equals("C1"))
    frequency = PRIMO_NOTE_C1;
  else if (cmdArgs.equals("CS1"))
    frequency = PRIMO_NOTE_CS1;
  else if (cmdArgs.equals("D1"))
    frequency = PRIMO_NOTE_D1;
  else if (cmdArgs.equals("DS1"))
    frequency = PRIMO_NOTE_DS1;
  else if (cmdArgs.equals("E1"))
    frequency = PRIMO_NOTE_E1;
  else if (cmdArgs.equals("F1"))
    frequency = PRIMO_NOTE_F1;
  else if (cmdArgs.equals("FS1"))
    frequency = PRIMO_NOTE_FS1;
  else if (cmdArgs.equals("G1"))
    frequency = PRIMO_NOTE_G1;
  else if (cmdArgs.equals("GS1"))
    frequency = PRIMO_NOTE_GS1;
  else if (cmdArgs.equals("A1"))
    frequency = PRIMO_NOTE_A1;
  else if (cmdArgs.equals("AS1"))
    frequency = PRIMO_NOTE_AS1;
  else if (cmdArgs.equals("B1"))
    frequency = PRIMO_NOTE_B1;

  else if (cmdArgs.equals("C2"))
    frequency = PRIMO_NOTE_C2;
  else if (cmdArgs.equals("CS2"))
    frequency = PRIMO_NOTE_CS2;
  else if (cmdArgs.equals("D2"))
    frequency = PRIMO_NOTE_D2;
  else if (cmdArgs.equals("DS2"))
    frequency = PRIMO_NOTE_DS2;
  else if (cmdArgs.equals("E2"))
    frequency = PRIMO_NOTE_E2;
  else if (cmdArgs.equals("F2"))
    frequency = PRIMO_NOTE_F2;
  else if (cmdArgs.equals("FS2"))
    frequency = PRIMO_NOTE_FS2;
  else if (cmdArgs.equals("G2"))
    frequency = PRIMO_NOTE_G2;
  else if (cmdArgs.equals("GS2"))
    frequency = PRIMO_NOTE_GS2;
  else if (cmdArgs.equals("A2"))
    frequency = PRIMO_NOTE_A2;
  else if (cmdArgs.equals("AS2"))
    frequency = PRIMO_NOTE_AS2;
  else if (cmdArgs.equals("B2"))
    frequency = PRIMO_NOTE_B2;

  else if (cmdArgs.equals("C3"))
    frequency = PRIMO_NOTE_C3;
  else if (cmdArgs.equals("CS3"))
    frequency = PRIMO_NOTE_CS3;
  else if (cmdArgs.equals("D3"))
    frequency = PRIMO_NOTE_D3;
  else if (cmdArgs.equals("DS3"))
    frequency = PRIMO_NOTE_DS3;
  else if (cmdArgs.equals("E3"))
    frequency = PRIMO_NOTE_E3;
  else if (cmdArgs.equals("F3"))
    frequency = PRIMO_NOTE_F3;
  else if (cmdArgs.equals("FS3"))
    frequency = PRIMO_NOTE_FS3;
  else if (cmdArgs.equals("G3"))
    frequency = PRIMO_NOTE_G3;
  else if (cmdArgs.equals("GS3"))
    frequency = PRIMO_NOTE_GS3;
  else if (cmdArgs.equals("A3"))
    frequency = PRIMO_NOTE_A3;
  else if (cmdArgs.equals("AS3"))
    frequency = PRIMO_NOTE_AS3;
  else if (cmdArgs.equals("B3"))
    frequency = PRIMO_NOTE_B3;

  else if (cmdArgs.equals("C4"))
    frequency = PRIMO_NOTE_C4;
  else if (cmdArgs.equals("CS4"))
    frequency = PRIMO_NOTE_CS4;
  else if (cmdArgs.equals("D4"))
    frequency = PRIMO_NOTE_D4;
  else if (cmdArgs.equals("DS4"))
    frequency = PRIMO_NOTE_DS4;
  else if (cmdArgs.equals("E4"))
    frequency = PRIMO_NOTE_E4;
  else if (cmdArgs.equals("F4"))
    frequency = PRIMO_NOTE_F4;
  else if (cmdArgs.equals("FS4"))
    frequency = PRIMO_NOTE_FS4;
  else if (cmdArgs.equals("G4"))
    frequency = PRIMO_NOTE_G4;
  else if (cmdArgs.equals("GS4"))
    frequency = PRIMO_NOTE_GS4;
  else if (cmdArgs.equals("A4"))
    frequency = PRIMO_NOTE_A4;
  else if (cmdArgs.equals("AS4"))
    frequency = PRIMO_NOTE_AS4;
  else if (cmdArgs.equals("B4"))
    frequency = PRIMO_NOTE_B4;

  else if (cmdArgs.equals("C5"))
    frequency = PRIMO_NOTE_C5;
  else if (cmdArgs.equals("CS5"))
    frequency = PRIMO_NOTE_CS5;
  else if (cmdArgs.equals("D5"))
    frequency = PRIMO_NOTE_D5;
  else if (cmdArgs.equals("DS5"))
    frequency = PRIMO_NOTE_DS5;
  else if (cmdArgs.equals("E5"))
    frequency = PRIMO_NOTE_E5;
  else if (cmdArgs.equals("F5"))
    frequency = PRIMO_NOTE_F5;
  else if (cmdArgs.equals("FS5"))
    frequency = PRIMO_NOTE_FS5;
  else if (cmdArgs.equals("G5"))
    frequency = PRIMO_NOTE_G5;
  else if (cmdArgs.equals("GS5"))
    frequency = PRIMO_NOTE_GS5;
  else if (cmdArgs.equals("A5"))
    frequency = PRIMO_NOTE_A5;
  else if (cmdArgs.equals("AS5"))
    frequency = PRIMO_NOTE_AS5;
  else if (cmdArgs.equals("B5"))
    frequency = PRIMO_NOTE_B5;

  else if (cmdArgs.equals("C6"))
    frequency = PRIMO_NOTE_C6;
  else if (cmdArgs.equals("CS6"))
    frequency = PRIMO_NOTE_CS6;
  else if (cmdArgs.equals("D6"))
    frequency = PRIMO_NOTE_D6;
  else if (cmdArgs.equals("DS6"))
    frequency = PRIMO_NOTE_DS6;
  else if (cmdArgs.equals("E6"))
    frequency = PRIMO_NOTE_E6;
  else if (cmdArgs.equals("F6"))
    frequency = PRIMO_NOTE_F6;
  else if (cmdArgs.equals("FS6"))
    frequency = PRIMO_NOTE_FS6;
  else if (cmdArgs.equals("G6"))
    frequency = PRIMO_NOTE_G6;
  else if (cmdArgs.equals("GS6"))
    frequency = PRIMO_NOTE_GS6;
  else if (cmdArgs.equals("A6"))
    frequency = PRIMO_NOTE_A6;
  else if (cmdArgs.equals("AS6"))
    frequency = PRIMO_NOTE_AS6;
  else if (cmdArgs.equals("B6"))
    frequency = PRIMO_NOTE_B6;

  else if (cmdArgs.equals("C7"))
    frequency = PRIMO_NOTE_C7;
  else if (cmdArgs.equals("CS7"))
    frequency = PRIMO_NOTE_CS7;
  else if (cmdArgs.equals("D7"))
    frequency = PRIMO_NOTE_D7;
  else if (cmdArgs.equals("DS7"))
    frequency = PRIMO_NOTE_DS7;
  else if (cmdArgs.equals("E7"))
    frequency = PRIMO_NOTE_E7;
  else if (cmdArgs.equals("F7"))
    frequency = PRIMO_NOTE_F7;
  else if (cmdArgs.equals("FS7"))
    frequency = PRIMO_NOTE_FS7;
  else if (cmdArgs.equals("G7"))
    frequency = PRIMO_NOTE_G7;
  else if (cmdArgs.equals("GS7"))
    frequency = PRIMO_NOTE_GS7;
  else if (cmdArgs.equals("A7"))
    frequency = PRIMO_NOTE_A7;
  else if (cmdArgs.equals("AS7"))
    frequency = PRIMO_NOTE_AS7;
  else if (cmdArgs.equals("B7"))
    frequency = PRIMO_NOTE_B7;

  else if (cmdArgs.equals("C8"))
    frequency = PRIMO_NOTE_C8;
  else if (cmdArgs.equals("CS8"))
    frequency = PRIMO_NOTE_CS8;
  else if (cmdArgs.equals("D8"))
    frequency = PRIMO_NOTE_D8;
  else if (cmdArgs.equals("DS8"))
    frequency = PRIMO_NOTE_DS8;

  if (frequency == 0)
    return false;

  if (dryRun)
    return true;

  playTone(frequency);
  return true;
}

////////////////////////////////////////////////////////////////////////////////

bool muteBuzzer (bool dryRun)
{
  if (dryRun)
    return true;

  noTone(PRIMO_BUZZER_PIN);

  return true;
}

////////////////////////////////////////////////////////////////////////////////

bool spinMotorClockwise (String cmdArgs, bool dryRun)
{
  char motor = cmdArgs.charAt(0);
  long steps = atol(cmdArgs.substring(1).c_str());

  if (steps == 0L)
    return false;

  if (motor == 'L')
  {
    if (!dryRun)
    {
      playTone(5000, 50);
      leftStepper.move(steps);
      runBothSteppers();
    }

    return true;
  }
  else if (motor == 'R')
  {
    if (!dryRun)
    {
      playTone(5000, 50);
      rightStepper.move(steps);
      runBothSteppers();
    }
  
    return true;
  }
  else
    return false;
}

////////////////////////////////////////////////////////////////////////////////

bool moveForward (String cmdArgs, bool dryRun)
{
  long steps = atol(cmdArgs.c_str());

  if (steps == 0L)
    return false;

  if (dryRun)
    return true;

  playTone(3000, 50);
  leftStepper.move(-steps);
  rightStepper.move(steps);
  runBothSteppers();

  return true;
}

////////////////////////////////////////////////////////////////////////////////

bool moveBackwards (String cmdArgs, bool dryRun)
{
  long steps = atol(cmdArgs.c_str());

  if (steps == 0L)
    return false;

  if (dryRun)
    return true;

  playTone(3000, 50);
  leftStepper.move(steps);
  rightStepper.move(-steps);
  runBothSteppers();

  return true;
}

////////////////////////////////////////////////////////////////////////////////

bool rotateLeft (String cmdArgs, bool dryRun)
{
  long steps = atol(cmdArgs.c_str());

  if (steps == 0L)
    return false;

  if (dryRun)
    return true;

  playTone(5000, 50);
  leftStepper.move(-steps);
  rightStepper.move(-steps);
  runBothSteppers();

  return true;
}

////////////////////////////////////////////////////////////////////////////////

bool rotateRight (String cmdArgs, bool dryRun)
{
  long steps = atol(cmdArgs.c_str());

  if (steps == 0L)
    return false;

  if (dryRun)
    return true;

  playTone(4000, 50);
  leftStepper.move(steps);
  rightStepper.move(steps);
  runBothSteppers();

  return true;
}

////////////////////////////////////////////////////////////////////////////////

bool getSensorsReadings (bool dryRun)
{
  if (dryRun)
    return true;

  commandReturnValue = String("#A0" + String(analogRead(A0)) + "#A1" + String(analogRead(A1)) + "#A2" + String(analogRead(A2)));

  return true;
}


////////////////////////////////////////////////////////////////////////////////

bool playConnectedTune (bool dryRun)
{
  if (dryRun)
    return true;

  tone(PRIMO_BUZZER_PIN, PRIMO_NOTE_G6);
  delay(100);
  tone(PRIMO_BUZZER_PIN, PRIMO_NOTE_B6);
  delay(100);
  tone(PRIMO_BUZZER_PIN, PRIMO_NOTE_C7);
  delay(100);
  noTone(PRIMO_BUZZER_PIN);

  return true;
}

////////////////////////////////////////////////////////////////////////////////

void runBothSteppers()
{
  leftStepper.enableOutputs();
  rightStepper.enableOutputs();

  bool leftStepperIsRunning, rightStepperIsRunning;

  do
  {
    // Run both steppers
    leftStepperIsRunning = leftStepper.run();
    rightStepperIsRunning = rightStepper.run();
  }
  while (leftStepper.run() || rightStepper.run());

  leftStepper.disableOutputs();
  rightStepper.disableOutputs();
}

////////////////////////////////////////////////////////////////////////////////

void checkInactivity()
{
  if (isInactive())
  {
    inactiveSignal();
    lastActivityTimestamp = millis();
  }
}

////////////////////////////////////////////////////////////////////////////////

void inactiveSignal()
{
  leftStepper.move(-PRIMO_STEPPER_TURN_STEPS / 2);
  rightStepper.move(-PRIMO_STEPPER_TURN_STEPS / 2);
  runBothSteppers();

  playSadTune();

  leftStepper.move(PRIMO_STEPPER_TURN_STEPS / 2);
  rightStepper.move(PRIMO_STEPPER_TURN_STEPS / 2);
  runBothSteppers();
}

////////////////////////////////////////////////////////////////////////////////

boolean isInactive()
{
  return (millis() - lastActivityTimestamp > PRIMO_CUBETTO_ROBOT_MAX_IDLE_TIME);
}

