// Primo - Cubetto Playset
//    Bare-bones code for the Radio Bridge

// Acts as a transparent short message sender/receiver between the USB port and
// the remote Cubetto Robot.

// This code includes a mechanism for Radio Bridge/Cubetto Robot to pair.
// When a Cubetto Robot is powered-on, it is unpaired and will accept input from
// any Radio Bridge.  When a Radio Bridge is powered on, it generates a 32-bit
// random number to use as its session Unique ID (UID).
// Every radio message that a Radio Bridge sends includes this UID.
// A Cubetto Robot, on receiving its first radio message, records this UID,
// and subsequently ignores messages from any other UID.
// A devices's UID is forgotten when it is powered off.

// Normal usage of Radio Bridge/Cubetto Robot:
// 1. Power-on the Radio Bridge.
// 2. Power-on Cubetto Robot.
// 3. Send a message from the Radio Bridge.
// 4. These devices are now paired.
// 5. Repeat 1, 2 & 3 for any other Radio Bridge & Cubetto Robots.
// 6. Power-off a Cubetto Robot to un-pair (the Radio Bridge is unaware of
//    pairings, so no need to power-off).

// @author Danilo Di Cuia - danilo@primo.io
// @author Claudio Indellicati - bitron.it@gmail.com
// @author Matteo Loglio - matteo@primo.io

////////////////////////////////////////////////////////////////////////////////

#include <SPI.h>
#include <nRF24L01.h>
#include <RF24.h>
#include <printf.h>
#include <limits.h>

#include "Primo.h"

////////////////////////////////////////////////////////////////////////////////

//#define PRIMO_DEBUG_MODE

#ifdef PRIMO_DEBUG_MODE
#define debugPrintf printf
#define debugMessage dumpMessage
#else
#define debugPrintf(...) ((void) 0)
#define debugMessage(...) ((void) 0)
#endif

////////////////////////////////////////////////////////////////////////////////

// Set up nRF24L01 radio on SPI bus pins 0 (CE) and 1 (CSN)
RF24 radio(0, 1);

// Flags to signal between interrupt handler and main event loop
volatile bool noMessageReceived = true;
volatile bool ackReceived = true;

RadioMessage receivedMsg;

#define PRIMO_ACK_RX_TIMEOUT 2000UL

////////////////////////////////////////////////////////////////////////////////

void setup()
{
  Serial.begin(115200);
  printf_begin();
  debugPrintf("Cubetto Playset - Radio Bridge\nVersion %s\n", PRIMO_CUBETTO_PLAYSET_VERSION);

  //
  // Session UID initialization
  //

  // Set the random number that will be used to uniquely identify THIS
  // communication session.  Note that random() actually returns a
  // pseudo-random sequence.  randomSeed() ensures that each device initialises
  // its session ID to a fairly random noise source
  randomSeed(analogRead(0) + analogRead(1) + analogRead(2) + analogRead(3) + analogRead(4) + analogRead(5));
  sessionId = random(1, LONG_MAX);

  //
  // Setup and configure RF radio module
  //
 
  radio.begin();
  //radio.setPALevel(RF24_PA_LOW);

  radio.enableDynamicPayloads();

  // Open pipes to other node for communication
  radio.openWritingPipe(PRIMO_RADIOBRIDGE2CUBETTO_PIPE_ADDRESS);
  radio.openReadingPipe(1, PRIMO_CUBETTO2RADIOBRIDGE_PIPE_ADDRESS); 

  radio.startListening();
 
  // Dump the configuration of the RF module for debugging
  radio.printDetails();
  delay(50);

  // Attach interrupt handler to interrupt #1 (using pin 2 on Arduino Micro)
  attachInterrupt(1, checkRadio, LOW);
}

////////////////////////////////////////////////////////////////////////////////

void loop()
{
  if (Serial.available())
  {
    String cmdFromSerial = Serial.readStringUntil('\n');
    cmdFromSerial.trim();

    if (cmdFromSerial.length() > PRIMO_MAX_MESSAGE_SIZE)
      cmdFromSerial = cmdFromSerial.substring(0, PRIMO_MAX_MESSAGE_SIZE);

    sendMessage(cmdFromSerial);
  }

  if (noMessageReceived)
    return;

  Serial.print((char) receivedMsg.code);

  for (int idx = 0; idx < PRIMO_MAX_MESSAGE_PAYLOAD_SIZE; ++idx)
  {
    Serial.print((char) receivedMsg.payload[idx]);

    if (receivedMsg.payload[idx] == (uint8_t) ' ')
      break;
  }

  Serial.print('\n');

  noMessageReceived = true;
}

